#ifndef STYLE_TRANSFER_TORCH_IMPL_TORCH_DATA_HPP
#define STYLE_TRANSFER_TORCH_IMPL_TORCH_DATA_HPP

#include <string>
#include <cuda_runtime.h>
#include <torch/torch.h>
#include <torch/script.h>

namespace Morph { namespace TorchImpl {

struct TorchData
{
    torch::Tensor texture;
    torch::PackedTensorAccessor64<float, 3> cudaAccessor;
    float* cudaData;
    torch::jit::script::Module model;

    TorchData(torch::Tensor _texture, std::string _model) 
        : texture(_texture), cudaAccessor(texture.packed_accessor64<float,3>())
    {
        cudaMallocManaged(&cudaData, texture.size(0)*texture.size(1)*texture.size(2)*sizeof(float));
        try {
            model = torch::jit::load(_model);
            std::cout << "model loaded" << std::endl;
        }
        catch (const c10::Error& e) {
            std::cout << "error loading the model\n";
            std::cout << e.what() << std::endl;
            exit(1);
        }
        model.to(torch::kCUDA);
    }
    ~TorchData()
    {
        cudaFree(cudaData);
    }
};

}}

#endif // STYLE_TRANSFER_TORCH_IMPL_TORCH_DATA_HPP