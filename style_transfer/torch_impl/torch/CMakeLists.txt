cmake_minimum_required(VERSION 3.11...3.16)

project(RealtimeStyleTransfer3D_TorchImpl
    VERSION 0.1
    DESCRIPTION "Torch helper implementation of realtime style transfer"
    LANGUAGES CXX C CUDA)

set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CUDA_STANDARD 14)

file(GLOB_RECURSE TORCH_IMPL_SRC
"src/**.h"
"src/**.hpp"
"src/**.cpp"
"src/**.cc"
"src/**.cu"
"src/**.cuh"
)

set(TORCH_IMPL_INCLUDE 
"src"
"../../../extern/morphengine/extern/stb"
"../../../extern/morphengine/extern/fmt/include"
"../../../extern/morphengine/extern/spdlog/include"
)

add_library(torch_impl STATIC ${TORCH_IMPL_SRC})
target_compile_options(torch_impl PRIVATE $<$<COMPILE_LANGUAGE:CXX>:${TORCH_CXX_FLAGS}>)
#set_target_properties(torch_impl PROPERTIES CUDA_SEPARABLE_COMPILATION ON)

target_include_directories(torch_impl PUBLIC ${TORCH_IMPL_INCLUDE})

target_link_libraries(torch_impl PRIVATE "${TORCH_LIBRARIES}")
#target_link_libraries(torch_impl PRIVATE fmt)
#target_link_libraries(torch_impl PRIVATE spdlog)

#message("torch flags: ${TORCH_CXX_FLAGS}")
#message("torch libs: ${TORCH_LIBRARIES}")
