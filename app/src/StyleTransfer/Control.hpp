#ifndef STYLE_TRANSFER_APP_CONTROL_HPP
#define STYLE_TRANSFER_APP_CONTROL_HPP

#include "Scene.hpp"
#include "GUI.hpp"

namespace Morph {

class StyleTransferControl
{
    class Screenshot {
        bool m_shouldMakeOne;
        cref<Texture2D> m_texture;
        string m_name;
        int m_screenshotNum;
    public:
        Screenshot(const Texture2D& texture, string name);
        void ShouldMakeOne();
        void MaybeMakeOne();
    };
private:
    ref<WindowManager> m_windowManager;
    ref<Window> m_window;
    ref<StyleTransferScene> m_scene;
    ref<StyleTransferGUI> m_gui;
    ref<bool> m_updateTransform;
    ref<bool> m_forceUpdateTransform;
    MethodAttacher<KeyEvent, StyleTransferControl> m_keyEventAttacher;
    // control variables
    float m_moveSpeed = 1;
    float m_rotSpeed = 1;
    bool m_isMoving = false;
    float m_moveTimeTolerance = 0;
    Screenshot m_inScreenshot;
    Screenshot m_outScreenshot;
    Screenshot m_normalsScreenshot;
    Screenshot m_lightDirScreenshot;
    Screenshot m_maskScreenshot;
    array<ref<Screenshot>, 5> m_screenshots;
public:
    StyleTransferControl(
        WindowManager& windowManager,
        Window& window,
        StyleTransferScene& scene,
        StyleTransferGUI& gui,
        bool& updateTransform,
        bool& forceUpdateTransform);
    void Update(f64 lastIterTime, f64 lastFrameTime);
private:
    void OnKeyEvent(const KeyEvent& event);
};

}


#endif // STYLE_TRANSFER_APP_CONTROL_HPP