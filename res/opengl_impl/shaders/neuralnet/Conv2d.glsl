#version 450 core

layout(local_size_x = GROUP_SIZE_X, local_size_y = GROUP_SIZE_Y, local_size_z = GROUP_SIZE_Z) in;

restrict layout (packed, binding = 0) buffer l_weightsLayout
{
    float u_weights[IN_CHANNELS * OUT_CHANNELS * KERNEL_SIZE_X * KERNEL_SIZE_Y];
};
#if USE_BIAS
layout (packed, binding = 1) uniform l_biasesLayout
{
    float u_biases[OUT_CHANNELS];
};
#endif

restrict readonly layout(r32f) uniform image3D u_input_data;
restrict writeonly layout(r32f) uniform image3D u_output_data;

#define INPUT_CACHE_STRIDE_ADD_X ((GROUP_SIZE_X - 1) * (STRIDE_X - 1))
#define INPUT_CACHE_STRIDE_ADD_Y ((GROUP_SIZE_Y - 1) * (STRIDE_Y - 1))
#define INPUT_CACHE_SIZE_X (GROUP_SIZE_X + INPUT_CACHE_STRIDE_ADD_X + KERNEL_SIZE_X - 1)
#define INPUT_CACHE_SIZE_Y (GROUP_SIZE_Y + INPUT_CACHE_STRIDE_ADD_Y + KERNEL_SIZE_Y - 1)
#define INPUT_CACHE_SIZE (INPUT_CACHE_SIZE_X * INPUT_CACHE_SIZE_Y)

shared float s_input[INPUT_CACHE_SIZE];
#define WEIGHTS_CACHE_SIZE (KERNEL_SIZE_X * KERNEL_SIZE_Y * OUT_CHANNELS)
shared float s_weights[WEIGHTS_CACHE_SIZE];

#define GROUP_SIZE (GROUP_SIZE_X * GROUP_SIZE_Y)
#define STRIDE (ivec2(STRIDE_X, STRIDE_Y))
#define KERNEL_OFFSET ivec2(KERNEL_SIZE_X/2,KERNEL_SIZE_Y/2)

#define DIVIDE_UPPER(a, d) ((a + d - 1) / d)

void main() {

    float out_values[OUT_CHANNELS];
    int local_id = int(gl_LocalInvocationIndex);
    ivec2 local_out_pos = ivec2(gl_LocalInvocationID.xy);
    ivec2 local_in_pos = local_out_pos * STRIDE;
    ivec2 out_pos = ivec2(gl_GlobalInvocationID.xy);
    ivec2 in_pos = out_pos * STRIDE;
    ivec2 out_group_pos = ivec2(gl_WorkGroupID.xy) * ivec2(GROUP_SIZE_X, GROUP_SIZE_Y);
    ivec2 in_group_pos = out_group_pos * STRIDE;

    // initialize outputs
    for(int out_channel = 0; out_channel < OUT_CHANNELS; ++out_channel) {
        out_values[out_channel] = 0;
    }

    for(int in_channel = 0; in_channel < IN_CHANNELS; ++in_channel) {
        
        barrier();
        // chache inputs
        for(int i = local_id; i < INPUT_CACHE_SIZE; i += GROUP_SIZE) {
            int y = i / INPUT_CACHE_SIZE_X;
            int x = i - y*INPUT_CACHE_SIZE_X;
            ivec2 offset = ivec2(x, y) - KERNEL_OFFSET;
            s_input[i] = imageLoad(u_input_data, ivec3(in_group_pos + offset, in_channel)).x;
        }
        // chache weights
        for(int i = local_id; i < WEIGHTS_CACHE_SIZE; i += GROUP_SIZE) {
            s_weights[i] = u_weights[i + KERNEL_SIZE_X * KERNEL_SIZE_Y * OUT_CHANNELS * in_channel];
        }
        barrier();

        // apply kernel
        for(int out_channel = 0; out_channel < OUT_CHANNELS; ++out_channel) {
            for(int y = 0; y < KERNEL_SIZE_Y; ++y) {
                for(int x = 0; x < KERNEL_SIZE_X; ++x) {
                    ivec2 pos = local_in_pos + ivec2(x,y);
                    float in_value = s_input[pos.x + pos.y * INPUT_CACHE_SIZE_X];
                    float weight = s_weights[x + KERNEL_SIZE_X * (y + KERNEL_SIZE_Y * out_channel)];
                    out_values[out_channel] += in_value * weight;
                }
            }
        }
    }


    // store outputs
    for(int out_channel = 0; out_channel < OUT_CHANNELS; ++out_channel) {
        #if USE_BIAS
            out_values[out_channel] += u_biases[out_channel];
        #endif
        /*if(local_in_pos.x == 0 || local_in_pos.y == 0) {
            out_values[out_channel] = 0;
        }*/
        imageStore(u_output_data, ivec3(out_pos, out_channel), vec4(out_values[out_channel]));
    }
}
