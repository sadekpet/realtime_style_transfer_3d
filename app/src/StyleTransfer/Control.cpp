#include "Control.hpp"

#include <Resource/ResourceManager.hpp>

#include <Window/Manager.hpp>

namespace Morph {

StyleTransferControl::StyleTransferControl(
    WindowManager& windowManager,
    Window& window,
    StyleTransferScene& scene,
    StyleTransferGUI& gui,
    bool& updateTransform,
    bool& forceUpdateTransform)
    : m_windowManager(windowManager),
    m_window(window),
    m_scene(scene),
    m_gui(gui),
    m_updateTransform(updateTransform),
    m_forceUpdateTransform(forceUpdateTransform),
    m_keyEventAttacher(&windowManager, this, &StyleTransferControl::OnKeyEvent),
    m_inScreenshot(scene.inTexture(), "in"),
    m_outScreenshot(scene.outTexture(), "out"),
    m_normalsScreenshot(scene.normalsTexture(), "normals"),
    m_lightDirScreenshot(scene.lightDirTexture(), "lightDir"),
    m_maskScreenshot(scene.maskTexture(), "mask"),
    m_screenshots({
        m_inScreenshot,
        m_outScreenshot,
        m_normalsScreenshot,
        m_lightDirScreenshot,
        m_maskScreenshot
    })
{
}

void StyleTransferControl::Update(f64 lastIterTime, f64 lastFrameTime)
{
    float deltaTime = (float)lastIterTime;
    float aspectRatio = m_scene->aspectRatio();

    glm::quat cameraRot = glm::angleAxis(
        m_scene->cameraData().transform.rotAngle,
        m_scene->cameraData().transform.rotAxis
    );
    vec3 leftDir = glm::rotate(cameraRot, vec3(1,0,0));
    vec3 forwardDir = glm::rotate(cameraRot, vec3(0,0,-1));

    bool moved = false;

    if(m_window->IsKeyPressed(Key::A)) {
        m_scene->cameraData().transform.pos -= leftDir * m_moveSpeed * deltaTime;
        moved = true;
    }
    if(m_window->IsKeyPressed(Key::D)) {
        m_scene->cameraData().transform.pos += leftDir * m_moveSpeed * deltaTime;
        moved = true;
    }
    if(m_window->IsKeyPressed(Key::S)) {
        m_scene->cameraData().transform.pos -= forwardDir * m_moveSpeed * deltaTime;
        moved = true;
    }
    if(m_window->IsKeyPressed(Key::W)) {
        m_scene->cameraData().transform.pos += forwardDir * m_moveSpeed * deltaTime;
        moved = true;
    }
    if(m_window->IsKeyPressed(Key::Q)) {
        m_scene->cameraData().transform.rotAngle += m_rotSpeed * deltaTime;
        moved = true;
    }
    if(m_window->IsKeyPressed(Key::E)) {
        m_scene->cameraData().transform.rotAngle -= m_rotSpeed * deltaTime;
        moved = true;
    }
    if(m_gui->valuesChanged()) {
        moved = true;
    }

    if(moved) {
        m_isMoving = true;
        m_moveTimeTolerance = 0.25f;
    }
    else if(m_isMoving && !moved) {
        if(m_moveTimeTolerance > 0) {
            m_moveTimeTolerance -= deltaTime;
        } else {
            m_isMoving = false;
            m_updateTransform = true;
        }
    }

    mat4 V = m_scene->cameraData().transform.ToInv();
    mat4 P = m_scene->cameraData().camera.ToMat(aspectRatio);
    m_scene->shaderTransform().Set(m_scene->objectData().transform.ToMat(), V, P);

    for(Screenshot& screenshot: m_screenshots) {
        screenshot.MaybeMakeOne();
    }
}

void StyleTransferControl::OnKeyEvent(const KeyEvent& event)
{
    if(event.action == KeyAction::PRESS) {
        if(event.key == Key::I) {
            m_inScreenshot.ShouldMakeOne();
        }
        if(event.key == Key::O) {
            m_outScreenshot.ShouldMakeOne();
        }
        if(event.key == Key::M) {
            m_maskScreenshot.ShouldMakeOne();
        }
        if(event.key == Key::N) {
            m_normalsScreenshot.ShouldMakeOne();
        }
        if(event.key == Key::L) {
            m_lightDirScreenshot.ShouldMakeOne();
        }

        if(event.key == Key::P) {
            // screenshot all
            for(Screenshot& screenshot: m_screenshots) {
                screenshot.ShouldMakeOne();
            }
        }


        if(event.key == Key::G) {
            m_gui->show() = !m_gui->show();
        }

        if(event.key == Key::V) {
            m_gui->lazyUpdate() = !m_gui->lazyUpdate();
        }
        if(event.key == Key::B) {
            m_gui->noUpdates() = !m_gui->noUpdates();
        }
        if(event.key == Key::F) {
            m_forceUpdateTransform = true;
        }
    }
}


StyleTransferControl::Screenshot::Screenshot(const Texture2D& texture, string name)
    : m_shouldMakeOne(false), m_texture(texture), m_name(name), m_screenshotNum(0)
{
}
void StyleTransferControl::Screenshot::ShouldMakeOne()
{
    m_shouldMakeOne = true;
}
void StyleTransferControl::Screenshot::MaybeMakeOne()
{
    if(m_shouldMakeOne) {
        m_shouldMakeOne = false;
        string shotName = m_name + std::to_string(m_screenshotNum) + ".png";
        spdlog::info("screenshot saved as {}", shotName);
        ResourceManager::SaveTexture2D_PNG(m_texture, shotName);
        m_screenshotNum++;
    }
}

}