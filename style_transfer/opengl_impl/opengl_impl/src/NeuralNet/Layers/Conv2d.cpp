#include "Conv2d.hpp"

#include <NeuralNet/Utils.hpp>

using namespace style_transfer;

namespace Morph
{

Conv2dProgram::Conv2dProgram(GraphicsContext& context, NeuralNetResources& resources, const style_transfer::Conv2d& conv2d, bool transpose)
    :
    m_conv2dID({
        conv2d.in_channels(), 
        conv2d.out_channels(), 
        conv2d.kernel_size_x(), 
        conv2d.kernel_size_x(),
        conv2d.stride_x(),
        conv2d.stride_y(),
        conv2d.use_bias()}),
    m_uniforms(unique<Conv2dUniforms>(new Conv2dUniforms())),
    m_inputUnitUniform("u_input_data", m_uniforms->inputUnit),
    m_outputUnitUniform("u_output_data", m_uniforms->outputUnit),
    m_program(resources.GetConv2dProgram(m_conv2dID))
{
    std::vector<float> weights(conv2d.weights_size());
    for(int in_channel = 0; in_channel < conv2d.in_channels(); ++in_channel) {
        for(int out_channel = 0; out_channel < conv2d.out_channels(); ++out_channel) {
            for(int y = 0; y < conv2d.kernel_size_y(); ++y) {
                for(int x = 0; x < conv2d.kernel_size_x(); ++x) {
                    int i_old = 0;
                    int i_new = 0;
                    if(transpose) {
                        i_old = y + conv2d.kernel_size_y()*(x + conv2d.kernel_size_x()*(in_channel + conv2d.in_channels() * out_channel));
                        i_new = x + conv2d.kernel_size_x()*(y + conv2d.kernel_size_y()*(out_channel + conv2d.out_channels() * in_channel));
                    } else {
                        i_old = x + conv2d.kernel_size_x()*(y + conv2d.kernel_size_y()*(in_channel + conv2d.in_channels() * out_channel));
                        i_new = x + conv2d.kernel_size_x()*(y + conv2d.kernel_size_y()*(out_channel + conv2d.out_channels() * in_channel));
                    }
                    weights[i_new] = conv2d.weights(i_old);
                }
            }
        }
    }

    m_weightsBuffer = context.CreateBufferRaw<GraphicsBufferTarget::SHADER_STORAGE,0,0,0>(
        sizeof(float)*weights.size(), weights.data()
    );
    if(m_conv2dID.use_bias){
        m_biasesBuffer = context.CreateBufferRaw<GraphicsBufferTarget::UNIFORM,0,0,0>(
            sizeof(float)*conv2d.biases_size(), conv2d.biases().data()
        );
    }
}

void Conv2dProgram::Evaluate(GraphicsContext& context, Texture3D& inputData, Texture3D& outputData)
{
    uvec3 dim = divide_upper(outputData.dim(), m_program.get().groupSize);
    dim.z = 1;
    ComputeProgramBinder programBinder = context.BindProgram(m_program.get().program);
    m_inputUnitUniform.Set(programBinder);
    m_outputUnitUniform.Set(programBinder);
    m_weightsBuffer.SetBindBase(0);
    if(m_conv2dID.use_bias){
        m_biasesBuffer.SetBindBase(1);
    }
    ImageBinder<true, false> inputBinder = context.BindImage<true, false>(m_uniforms->inputUnit, inputData, true);
    ImageBinder<false, true> ouputBinder = context.BindImage<false, true>(m_uniforms->outputUnit, outputData, true);
    context.DispatchCompute(programBinder, dim);
}

}