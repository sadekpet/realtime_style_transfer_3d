
#include <gtest/gtest.h>
#include <NeuralNet/Layers/Tanh.hpp>
#include <GlobalState.hpp>
#include <TestingUtils.hpp>

using namespace Morph;

TEST(LayersTanh, doubleTexture) {
    TanhProgram tanhProgram(TestsGlobalState::neuralNetResources());

    array<float, 8> inputValues = {
        -100, -10, -5, -1, 0, 1, 5, 100
    };

    Texture3D inputData = TestsUtils::CreateTexture3D(uvec3(2), inputValues.data());
    Texture3D outputData = TestsUtils::CreateTexture3D(uvec3(2));

    tanhProgram.Evaluate(TestsGlobalState::context(), inputData, outputData);

    array<float, 8> actualOutputValues;
    array<float, 8> expectedOutputValues;

    outputData.GetData(actualOutputValues.data());
    std::transform(inputValues.begin(), inputValues.end(), expectedOutputValues.begin(), [](float x) { return std::tanh(x); });

    ASSERT_PRED3(TestsUtils::CompareFloatsN<8>, actualOutputValues, expectedOutputValues, TestsUtils::precision());
}