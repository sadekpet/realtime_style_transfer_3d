#include "Upsampling.hpp"

#include <NeuralNet/Utils.hpp>

namespace Morph
{

UpsamplingProgram::UpsamplingProgram(NeuralNetResources& resources, const style_transfer::Upsampling& upsampling)
    :
    m_uniforms(unique<UpsamplingUniforms>(new UpsamplingUniforms())),
    m_scale_factorUniform("u_scale_factor", m_uniforms->scale_factor),
    m_inputUnitUniform("u_input_data", m_uniforms->inputUnit),
    m_outputUnitUniform("u_output_data", m_uniforms->outputUnit),
    m_program(resources.GetUpsamplingProgram())
{
    m_uniforms->scale_factor = upsampling.scale_factor();
}

void UpsamplingProgram::Evaluate(GraphicsContext& context, Texture3D& inputData, Texture3D& outputData)
{
    uvec3 dim = divide_upper(outputData.dim(), m_program.get().groupSize);
    ComputeProgramBinder programBinder = context.BindProgram(m_program.get().program);
    m_scale_factorUniform.Set(programBinder);
    m_inputUnitUniform.Set(programBinder);
    m_outputUnitUniform.Set(programBinder);
    ImageBinder<true, false> inputBinder = context.BindImage<true, false>(m_uniforms->inputUnit, inputData, true);
    ImageBinder<false, true> ouputBinder = context.BindImage<false, true>(m_uniforms->outputUnit, outputData, true);
    context.DispatchCompute(programBinder, dim);
}

}