#version 450 core

layout(local_size_x = GROUP_SIZE_X, local_size_y = GROUP_SIZE_Y, local_size_z = GROUP_SIZE_Z) in;

uniform vec3 u_mean;
uniform vec3 u_std;

readonly layout(rgba8) uniform image2D u_input_data;
writeonly layout(r32f) uniform image3D u_output_data;

void main() {
    ivec2 pixel_coords = ivec2(gl_GlobalInvocationID.xy);
    ivec2 input_size = imageSize(u_input_data);
    if(pixel_coords.x < input_size.x && pixel_coords.y < input_size.y) {
        vec3 pixel = (imageLoad(u_input_data, pixel_coords).xyz - u_mean) / u_std;
        imageStore(u_output_data, ivec3(pixel_coords, 0), vec4(pixel.x));
        imageStore(u_output_data, ivec3(pixel_coords, 1), vec4(pixel.y));
        imageStore(u_output_data, ivec3(pixel_coords, 2), vec4(pixel.z));
    }
}