#include <iostream>
#include <fstream>

#include <spdlog/spdlog.h>

#include <Morph.hpp>
#include <Window/Manager.hpp>
#include <Graphics/Context.hpp>
#include <Graphics/Uniforms.hpp>
#include <Resource/GraphicsProgramCompiler.hpp>
#include <Resource/Common.hpp>
#include <Resource/ResourceManager.hpp>
#include <Data/ShaderTransform.hpp>
#include <Data/Transform.hpp>
#include <Data/Camera.hpp>
#include <Data/PBR.hpp>
#include <ImGui/ImGui.hpp>
#include <Profile/GraphicsTimer.hpp>

#include <limits>

#include <NeuralNet/ModelPrinter.hpp>
#include <NeuralNet/Pipeline.hpp>

using namespace magic_enum::ostream_operators;
using namespace style_transfer;

namespace Morph {

struct CameraData
{
    PerspectiveCamera3D camera;
    Transform transform;
};

struct ObjectData
{
    Transform transform;
};

class Application
{
private:
    unique<WindowManager> m_windowManager;
    MethodAttacher<WindowManagerError, Application> m_windowManagerErrorAttacher;
    MethodAttacher<WindowSizeEvent, Application> m_windowSizeEventAttacher;
    MethodAttacher<WindowFramebufferSizeEvent, Application> m_windowFramebufferSizeEventAttacher;
    MethodAttacher<KeyEvent, Application> m_keyEventAttacher;
    MethodAttacher<ScrollEvent, Application> m_scrollEventAttacher;
    WindowID m_windowID;
    unique<GraphicsContext> m_context;

    float m_renderTimer = 0;
    CameraData m_cameraData;
    ObjectData m_objectData;
    vec3 m_envGlobalAmbient = vec3(0.01);
    Material m_material;
    uint m_lightCount = 1;
    Light m_light;
    MaterialTex m_textures;
    bool m_showLight = true;
    float m_moveSpeed = 1;
    float m_rotSpeed = 1;

    bool m_makeScreenshot = false;
    int m_screenshotNum = 0;
    CustomFramebuffer m_sceneFramebuffer;
public:
    Application() {

        m_windowManager = unique<WindowManager>(WindowManager::Get());

        m_windowManagerErrorAttacher = {m_windowManager.get(), this, &Application::OnWindowManagerError};
        m_windowSizeEventAttacher = {m_windowManager.get(), this, &Application::OnWindowSizeEvent};
        m_keyEventAttacher = {m_windowManager.get(), this, &Application::OnKeyEvent};
        m_scrollEventAttacher = {m_windowManager.get(), this, &Application::OnScrollEvent};

        m_windowID = value_or_panic(m_windowManager->CreateWindow(ivec2(128,128), "style transfer demo"), "failed to create window");
    }

    void Run() {

        Window& window = value_or_panic(m_windowManager->GetWindow(m_windowID), "window unexpectedly destroyed");
        m_context = unique<GraphicsContext>(value_or_panic(GraphicsContext::MakeCurrent(window)));
        DefaultFramebuffer& defaultFramebuffer = m_context->GetDefaultFramebuffer();
        defaultFramebuffer.SetSwapWaitInterval(1);

        GraphicsSettings graphicsSettings = {
            MultisampleEnabledSetting(true),
            DepthTestEnabledSetting(true),
            CullFaceEnabledSetting(true),
            CullFaceSetting(CullFaceType::BACK),
            FrontFaceSetting(FrontFaceType::CCW),
            ClearColorSetting(0)
        };
        GraphicsSettingsApplier graphicsSettingsApplier = m_context->ApplySettings(graphicsSettings);

        ImGuiContext imguiContext(window);

        ResourceStorage resStorage;
        resStorage.InsertResFolder("engine", MORPH_ENGINE_RES);
        resStorage.InsertResFolder("shared", MORPH_SHARED_RES);
        GraphicsProgramCompiler progCompiler(*m_context, resStorage);
        CommonResources commonResources(*m_context, resStorage, progCompiler);
        NeuralNetResources neuralNetResources(progCompiler);

        GraphicsTimeElapsedQuery timeQuery = m_context->CreateQuery<GraphicsQueryType::TIME_ELAPSED>();

        ShaderTransform shaderTransform;
        ShaderTransformUniforms shaderTransformUniforms(shaderTransform);
        raw<Light> rawLights(1, &m_light);
        PBRForwardUniforms phongForwardUniforms(m_envGlobalAmbient, m_cameraData.transform.pos, m_material, m_lightCount, rawLights);
        PBRForwardTextureUnits phongForwardTextureUnits(m_textures, commonResources.WhiteTexture2D());
        Uniform<vec3> lightColorUniform("u_color", m_light.color);

        m_cameraData.transform.pos = vec3(-0.375, 0, 0.792);
        m_cameraData.transform.rotAxis = vec3(0, 1, 0);
        m_cameraData.transform.rotAngle = -0.425;
        m_objectData.transform.scale = vec3(0.5);
        m_material.albedo = vec3(0, 1, 1);

        m_light.lightType = LightType::POINT;
        m_light.position = vec4(0, 0, 1, 1);
        m_light.attenuationLin = 2;
        m_light.attenuationQuad = 3;

        TextureSettings sceneTextureSettings = {
            TextureMinFilterSetting(TextureMinFilter::NEAREST),
            TextureMagFilterSetting(TextureMagFilter::NEAREST)
        };
        m_sceneFramebuffer = m_context->CreateFramebuffer();
        Texture2D sceneTexture = m_context->CreateTexture2D(window.GetSize(), TextureSizedFormat::RGBA8, sceneTextureSettings);
        Renderbuffer sceneRenderbuffer = m_context->CreateRenderbuffer(window.GetSize(), TextureSizedFormat::DEPTH24_STENCIL8);
        m_sceneFramebuffer.Attach(CustomFramebufferAttachment::COLOR_0, sceneTexture);
        m_sceneFramebuffer.Attach(CustomFramebufferAttachment::DEPTH_STENCIL, sceneRenderbuffer);

        TextureUnit textureSamplerUnit = TextureUnit::_0;
        Uniform<TextureUnit> textureSamplerUniform("u_textureSampler", textureSamplerUnit);
        /*
        style_transfer::Add add_layer;
        add_layer.set_id1(0);
        add_layer.set_id2(1);

        style_transfer::Cat2 cat2_layer;
        cat2_layer.set_id1(0);
        cat2_layer.set_id2(1);

        style_transfer::Upsampling upsampling_layer;
        upsampling_layer.set_scale_factor(2);*/

        NeuralNet model;
        google::protobuf::RepeatedPtrField<style_transfer::Layer>& layers = *model.mutable_layers();
        
        array<style_transfer::Layer*, 8> layerPtrs;
        for(int i = 0; i < layerPtrs.size(); i++) {
            layerPtrs[i] = layers.Add();
            layerPtrs[i]->set_id(i+1);
        }
        
        style_transfer::Conv2d& conv2d_layer = *layerPtrs[0]->mutable_conv2d();
        conv2d_layer.set_in_channels(3);
        conv2d_layer.set_out_channels(1);
        conv2d_layer.set_kernel_size_x(3);
        conv2d_layer.set_kernel_size_y(3);
        conv2d_layer.set_stride_x(2);
        conv2d_layer.set_stride_y(2);
        conv2d_layer.set_use_bias(true);
        google::protobuf::RepeatedField<float>& conv_weights = *conv2d_layer.mutable_weights();
        conv_weights.Resize(conv2d_layer.in_channels() * conv2d_layer.out_channels() * conv2d_layer.kernel_size_x() * conv2d_layer.kernel_size_y(), 1.0f / 27.0f);
        google::protobuf::RepeatedField<float>& conv_biases = *conv2d_layer.mutable_biases();
        conv_biases.Resize(conv2d_layer.out_channels(), 0.05f);

        style_transfer::ReLU& relu_layer = *layerPtrs[1]->mutable_relu();
        style_transfer::LeakyReLU& leakyrelu_layer = *layerPtrs[2]->mutable_leakyrelu();
        leakyrelu_layer.set_negative_slope(0.1);
        style_transfer::Tanh& tanh_layer = *layerPtrs[3]->mutable_tanh();
        style_transfer::BatchNorm2d& batchnorm2d_layer = *layerPtrs[4]->mutable_batchnorm2d();
        batchnorm2d_layer.set_num_features(conv2d_layer.out_channels());
        batchnorm2d_layer.set_eps(1e-05);
        batchnorm2d_layer.set_momentum(0.1f);
        batchnorm2d_layer.set_affine(true);
        google::protobuf::RepeatedField<float>& batchnorm2d_means = *batchnorm2d_layer.mutable_means();
        batchnorm2d_means.Resize(batchnorm2d_layer.num_features(), 0.0f);
        google::protobuf::RepeatedField<float>& batchnorm2d_vars = *batchnorm2d_layer.mutable_vars();
        batchnorm2d_vars.Resize(batchnorm2d_layer.num_features(), 1.0f);
        google::protobuf::RepeatedField<float>& batchnorm2d_gammas = *batchnorm2d_layer.mutable_gammas();
        batchnorm2d_gammas.Resize(batchnorm2d_layer.num_features(), 1.0f);
        google::protobuf::RepeatedField<float>& batchnorm2d_betas = *batchnorm2d_layer.mutable_betas();
        batchnorm2d_betas.Resize(batchnorm2d_layer.num_features(), 0.0f);
        style_transfer::Upsampling& upsampling_layer = *layerPtrs[5]->mutable_upsampling();
        upsampling_layer.set_scale_factor(2);
        style_transfer::Cat3& cat3_layer = *layerPtrs[6]->mutable_cat3();
        cat3_layer.set_id1(6);
        cat3_layer.set_id2(6);
        cat3_layer.set_id3(6);
        style_transfer::Add& add_layer = *layerPtrs[7]->mutable_add();
        add_layer.set_id1(0);
        add_layer.set_id2(7);
        


        NeuralNetPipeline netPipeline(*m_context, neuralNetResources, model, window.GetSize());

        //while(!window.ShouldClose())
        {
            float deltaTime = 1.0f / ImGui::GetIO().Framerate;
            float aspectRatio = (float) window.GetSize().x / (float) window.GetSize().y;
            mat4 V;
            mat4 P;

            {
                glm::quat cameraRot = glm::angleAxis(m_cameraData.transform.rotAngle, m_cameraData.transform.rotAxis);
                vec3 leftDir = glm::rotate(cameraRot, vec3(1,0,0));
                vec3 forwardDir = glm::rotate(cameraRot, vec3(0,0,-1));

                V = m_cameraData.transform.ToInv();
                P = m_cameraData.camera.ToMat(aspectRatio);
                shaderTransform.Set(m_objectData.transform.ToMat(), V, P);
            }

            {
                FramebufferBinder framebufferBinder = m_context->BindFramebuffer(m_sceneFramebuffer);

                framebufferBinder.Clear();
                m_context->SetViewport(m_context->GetDefaultFramebuffer().dim());
                // draw iterations
                {
                    RenderProgramBinder programBinder = m_context->BindProgram(commonResources.PBRForwardProgram());
                    shaderTransformUniforms.SetUniforms(programBinder);
                    phongForwardUniforms.SetUniforms(programBinder);
                    TextureUnitsBinder textureUnitsBinder = phongForwardTextureUnits.Bind(*m_context, programBinder);
                    commonResources.SphereMesh().Draw(*m_context, framebufferBinder, programBinder);
                }

                {
                    GraphicsQueryScope<GraphicsQueryType::TIME_ELAPSED> timeQueryScope = m_context->BeginQuery(timeQuery);
                    netPipeline.Evaluate(*m_context, sceneTexture, sceneTexture);
                }
                // show light
                if(m_showLight) 
                {
                    Transform lightTrans;
                    lightTrans.pos = m_light.position;
                    lightTrans.scale = vec3(0.05);
                    shaderTransform.Set(lightTrans.ToMat(), V, P);
                    RenderProgramBinder programBinder = m_context->BindProgram(commonResources.MeshFillProgram());
                    shaderTransformUniforms.SetUniforms(programBinder);
                    lightColorUniform.Set(programBinder);
                    commonResources.CubeMesh().Draw(*m_context, framebufferBinder, programBinder);
                }
            }

            {
                FramebufferBinder framebufferBinder = m_context->BindFramebuffer(defaultFramebuffer);
                framebufferBinder.Clear();
                RenderProgramBinder programBinder = m_context->BindProgram(commonResources.ScreenFillProgram());
                TextureUnitBinder textureUnitBinder = m_context->BindTextureUnit(textureSamplerUnit, sceneTexture);
                textureSamplerUniform.Set(programBinder);
                commonResources.ScreenQuad2DMesh().Draw(*m_context, framebufferBinder, programBinder);
            }

            //netPipeline.PrintMemoryUsage();
            std::cout << "total data memmory: " << (float)netPipeline.GetTotalMemoryUsage() / (1024*1024) << " MB" << std::endl;
            std::cout << "max data block memmory: " << (float)netPipeline.GetMaxBlockMemoryUsage() / (1024*1024) << " MB" << std::endl;

            m_renderTimer = (float)timeQuery.GetValueBlocking() / 1000000.0f;
            std::cout << m_renderTimer << " ms" << std::endl;
            

            ResourceManager::SaveTexture2D_PNG(sceneTexture, "output.png");
            // draw ui
            defaultFramebuffer.SwapBuffers();
            m_windowManager->PollEvents();
        }

    };

private:
    void OnWindowManagerError(const WindowManagerError& error) {
        spdlog::error("{}: {}", error.type, error.message);
    }
    void OnWindowSizeEvent(const WindowSizeEvent& event) {
        spdlog::trace("window resize {}", event.size);
        m_sceneFramebuffer.Resize(event.size);
    }
    void OnKeyEvent(const KeyEvent& event) {
        spdlog::trace("key {} {}", event.key, event.action);
        Window& window = value_or_panic(m_windowManager->GetWindow(m_windowID), "window unexpectedly destroyed");
        if(event.action == KeyAction::PRESS) {
            if(event.key == Key::O) {
                spdlog::set_level(spdlog::level::trace);
            }
            if(event.key == Key::P) {
                spdlog::set_level(spdlog::level::info);
            }
            if(event.key == Key::L) {
                spdlog::info("screenshot should be taken");
                m_makeScreenshot = true;
            }
        }
    }

    void OnScrollEvent(const ScrollEvent& event) {
        spdlog::trace("scroll {}", event.offset);
    }
};

}

int main(int, char**) {
    Morph::Application app;
    app.Run();
}
