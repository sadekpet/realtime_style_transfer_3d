#version 450 core

layout(local_size_x = GROUP_SIZE_X, local_size_y = GROUP_SIZE_Y, local_size_z = GROUP_SIZE_Z) in;

readonly layout(r32f) uniform image3D u_input_data;
writeonly layout(r32f) uniform image3D u_output_data;

void main() {
    ivec3 coord = ivec3(gl_GlobalInvocationID);
    float value = imageLoad(u_input_data, coord).x;
    value = max(0, value);
    imageStore(u_output_data, coord, vec4(value));
}
