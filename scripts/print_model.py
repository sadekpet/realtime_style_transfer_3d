import argparse
import os
import sys
import time
import re

import numpy as np
import torch
import torch.nn as nn
from torch.autograd import Variable
from torchvision import models

from models import GeneratorJ

# elemental blocks

class BasicPrinter:
    def __init__(self, model, basic=False, debug_names=False, norm_layer='batch_norm', tanh=True):
        self.model = model
        self.basic = basic
        self.debug_names = debug_names
        self.use_bias = model.use_bias
        self.resnet_blocks = model.resnet_blocks
        self.append_smoothers = model.append_smoothers
        self.norm_layer = norm_layer
        self.tanh = tanh
        self.opIndex = 0


    def print_Conv2d(self, conv2d, use_bias):
        print('Conv2d')
        print(conv2d.in_channels, conv2d.out_channels, conv2d.kernel_size[0], conv2d.kernel_size[1], conv2d.stride[0], conv2d.stride[1])
        if not self.basic:
            params = list(conv2d.parameters())
            # weights
            # size: out_channels, in_channels, kernel_size.x, kernel_size.y
            print(params[0].tolist())

            # biases
            # size: out_channels
            if use_bias:
                print(params[1].tolist())
        self.opIndex += 1

    def print_Upsampling(self):
        print('Upsampling')
        print(2)
        self.opIndex += 1

    def print_Tanh(self):
        print('Tanh')
        self.opIndex += 1

    def print_ReLU(self):
        print('ReLU')
        self.opIndex += 1

    def print_LeakyReLU(self, negative_slope):
        print('LeakyReLU')
        print(negative_slope)
        self.opIndex += 1

    def print_BatchNorm2d(self, batch_norm2d):
        print('BatchNorm2d')
        print(batch_norm2d.num_features, batch_norm2d.eps, batch_norm2d.momentum, int(batch_norm2d.affine))
        if not self.basic:
            print(batch_norm2d.running_mean.tolist())
            print(batch_norm2d.running_var.tolist())
            params = list(batch_norm2d.parameters())
            print(params[0].tolist())
            print(params[1].tolist())
        self.opIndex += 1

    def print_InstanceNorm2d(self, instance_norm2d):
        print('InstanceNorm2d')
        print(instance_norm2d.num_features, instance_norm2d.eps, instance_norm2d.momentum, instance_norm2d.affine)
        if not self.basic:
            params = list(instance_norm2d.parameters())
            print(params[0].tolist())
            print(params[1].tolist())
        self.opIndex += 1

    def print_Add(self, idx1, idx2):
        print('Add')
        print(idx1, idx2)
        self.opIndex += 1
    
    def print_Cat2(self, idx1, idx2, dim):
        print('Cat2')
        print(idx1, idx2, dim)
        self.opIndex += 1

    def print_Cat3(self, idx1, idx2, idx3, dim):
        print('Cat3')
        print(idx1, idx2, idx3, dim)
        self.opIndex += 1

    # sequence blocks

    def print_norm(self, layer):
        if self.norm_layer != None:
            if self.norm_layer == 'batch_norm':
                self.print_BatchNorm2d(layer)
            if self.norm_layer == 'instance_norm':
                self.print_InstanceNorm2d(layer)

    def print_relu_layer(self, layer):
        if self.debug_names:
            print('#relu_layer')
        self.print_Conv2d(layer.conv, self.use_bias)
        self.print_norm(layer.normalization)
        self.print_LeakyReLU(.2)

    def print_resnet_block(self, layer):
        if self.debug_names:
            print('#resnet_block')
        self.print_ReLU()
        self.print_Conv2d(layer.conv_0, self.use_bias)
        self.print_norm(layer.normalization)
        self.print_ReLU()
        self.print_Conv2d(layer.conv_1, self.use_bias)


    def print_upconv_layer_upsample_and_conv(self, layer):
        if self.debug_names:
            print('#upconv_layer_upsample_and_conv')
        self.print_Upsampling()
        self.print_Conv2d(layer[1], False)
        self.print_norm(layer[2])
        self.print_ReLU()

    def print_conv_11(self):
        if self.debug_names:
            print('#conv_11')
        self.print_Conv2d(self.model.conv_11[0], self.use_bias)
        self.print_ReLU()

    def print_conv_11_a(self):
        if self.append_smoothers:
            if self.debug_names:
                print('#conv_11_a')
            self.print_Conv2d(self.model.conv_11_a[0], self.use_bias)
            self.print_ReLU()
            self.print_BatchNorm2d(self.model.conv_11_a[2])
            self.print_Conv2d(self.model.conv_11_a[3], self.use_bias)
            self.print_ReLU()

    def print_conv_12(self):
        if self.debug_names:
            print('#conv_12')
        if self.tanh:
            self.print_Conv2d(self.model.conv_12[0], True)
            self.print_Tanh()
        else:
            self.print_Conv2d(self.model.conv_12, True)


    # whole net

    def print_model(self):
        self.print_relu_layer(self.model.conv0)
        conv0_idx = self.opIndex
        self.print_relu_layer(self.model.conv1)
        conv1_idx = self.opIndex
        self.print_relu_layer(self.model.conv2)
        conv2_idx = self.opIndex
        add_idx = self.opIndex
        for i in range(self.resnet_blocks):
            self.print_resnet_block(self.model.resnets[i])
            self.print_Add(add_idx, self.opIndex)
            add_idx = self.opIndex
        self.print_Cat2(conv2_idx, self.opIndex, 1)
        self.print_upconv_layer_upsample_and_conv(self.model.upconv2)
        self.print_Cat2(conv1_idx, self.opIndex, 1)
        self.print_upconv_layer_upsample_and_conv(self.model.upconv1)
        self.print_Cat3(conv0_idx, self.opIndex, 0, 1)
        self.print_conv_11()
        self.print_conv_11_a()
        self.print_conv_12()
    



if __name__ == "__main__":
    with torch.no_grad():
        style_model = torch.load("res/model_00012.pth")
        printer = BasicPrinter(style_model, True, True)
        #printer.print_model()
        params = list(style_model.conv0.conv.parameters())
        #print(style_model.conv0.conv)
        print(params[0].size())
        #params = list(style_model.parameters())
        #print(len(params))
        #print(style_model.input_size)
        #print(style_model.norm_layer)
        #params = list(style_model.conv0.conv.parameters())
        #print(torch.flatten(params[0]).tolist())
        #print(style_model.conv_12)