#ifndef MORPH_NEURAL_NET_LAYERS_OUTPUT_HPP
#define MORPH_NEURAL_NET_LAYERS_OUTPUT_HPP

#include <Core/Core.hpp>
#include <Graphics/Context.hpp>
#include <Graphics/Uniforms.hpp>

#include <NeuralNet/Resources.hpp>

namespace Morph
{

class NeuralNetOutputProgram
{
    struct OutputUniforms
    {
        vec3 mean;
        vec3 std;
        TextureUnit inputUnit = TextureUnit::_0;
        TextureUnit outputUnit = TextureUnit::_1;
    };
private:
    unique<OutputUniforms> m_uniforms;
    Uniform<vec3> m_meanUniform;
    Uniform<vec3> m_stdUniform;
    Uniform<TextureUnit> m_inputUnitUniform;
    Uniform<TextureUnit> m_outputUnitUniform;
    ref<LayerProgram> m_program;
public:
    NeuralNetOutputProgram(NeuralNetResources& resources, vec3 mean = vec3(0.5f), vec3 std = vec3(0.5f));

    void Evaluate(GraphicsContext& context, Texture3D& inputData, Texture2D& outputTexture);
};

}

#endif // MORPH_NEURAL_NET_LAYERS_OUTPUT_HPP