#version 450 core

layout(local_size_x = GROUP_SIZE_X, local_size_y = GROUP_SIZE_Y, local_size_z = GROUP_SIZE_Z) in;

uniform uint u_scale_factor;

readonly layout(r32f) uniform image3D u_input_data;
writeonly layout(r32f) uniform image3D u_output_data;

void main() {
    ivec3 out_coord = ivec3(gl_GlobalInvocationID);
    ivec3 in_coord = ivec3(out_coord.x / u_scale_factor, out_coord.y / u_scale_factor, out_coord.z);
    float value = imageLoad(u_input_data, in_coord).x;
    imageStore(u_output_data, out_coord, vec4(value));
}
