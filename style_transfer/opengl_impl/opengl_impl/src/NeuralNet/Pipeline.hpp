#ifndef MORPH_NEURAL_NET_PIPELINE_HPP
#define MORPH_NEURAL_NET_PIPELINE_HPP

#include <Core/Core.hpp>

#include "Layers.hpp"
#include "PipelineStorage.hpp"
#include "nndata.pb.h"

namespace Morph
{

class NeuralNetPipeline
{
private:
    NeuralNetInputProgram m_inputLayer;
    NeuralNetOutputProgram m_outputLayer;
    vector<NeuralNetLayerVar> m_layers;
    NeuralNetPipelineStorage m_storage;
public:
    NeuralNetPipeline(GraphicsContext& context, NeuralNetResources& resources, style_transfer::NeuralNet& neuralNet, uvec2 resolution);

    void Evaluate(GraphicsContext& context, Texture2D& inputTexture, Texture2D& outputTexture);

    u64 GetTotalMemoryUsage() const;
    u64 GetMaxBlockMemoryUsage() const;
    void PrintMemoryUsage() const;
private:
    tuple<vector<uvec3>, vector<vector<size_t>>> GetDimsAndDependencies(style_transfer::NeuralNet& neuralNet, uvec3 startDim);
    uvec3 GetLayerDim(const style_transfer::Layer &layer, uvec3 prevDim);
};

}

#endif // MORPH_NEURAL_NET_PIPELINE_HPP