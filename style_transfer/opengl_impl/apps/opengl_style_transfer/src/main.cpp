#include <fstream>
#include <StyleTransferApp.hpp>
#include <Resource/ResourceManager.hpp>
#include <Profile/GraphicsTimer.hpp>

#include <NeuralNet/ModelPrinter.hpp>
#include <NeuralNet/Pipeline.hpp>
#include <NeuralNet/BasicPipeline.hpp>

using namespace style_transfer;

namespace Morph {

class OpenglStyleTransferApp : public StyleTransferApp
{
private:
    // neural net
    NeuralNetResources m_neuralNetResources;
    string m_model_path;
    NeuralNet m_model;
    Void d_loadModel;
    NeuralNetPipeline m_netPipeline;
    // timers
    GraphicsTimeElapsedQuery m_timer;
    float m_time = 0;
public:
    OpenglStyleTransferApp(const WindowAppConfig& winConfig, const StyleTransferAppConfig& appConfig)
        : StyleTransferApp(winConfig, appConfig),
        m_neuralNetResources(progCompiler()),
        m_model_path(value_or_panic(
            resStorage().GetPath("shared:opengl_impl/to_red.nndata"),
            "resource path shared:opengl_impl/to_red.nndata does not exist"
        )),
        d_loadModel(MORPH_VOID(ParseModel())),
        m_netPipeline(context(), m_neuralNetResources, m_model, scene().inTexture().dim()),
        m_timer(context().CreateQuery<GraphicsQueryType::TIME_ELAPSED>())
    {
        scene().modelType() = StyleTransferScene::ModelType::SPHERE;
    }

    virtual ~OpenglStyleTransferApp()
    {
        ResourceManager::SaveTexture2D_PNG(scene().outTexture(), "output.png");

        m_netPipeline.PrintMemoryUsage();
        spdlog::info("total data memmory: {} MB", (float)m_netPipeline.GetTotalMemoryUsage() / (1024*1024));
        spdlog::info("max data block memmory: {} MB", (float)m_netPipeline.GetMaxBlockMemoryUsage() / (1024*1024));

        spdlog::info("eval time {}ms", m_time);
    }
protected:
    void Transform() override
    {
        {
            auto timeQueryScope = context().BeginQuery(m_timer);
            m_netPipeline.Evaluate(context(), scene().inTexture(), scene().outTexture());
        }
        m_time = (float)m_timer.GetValueBlocking() / 1000000.0f;
    }

    float TransformTime() override
    {
        return m_time;
    }
private:
    void ParseModel()
    {
        std::fstream input_file(m_model_path, std::ios::in | std::ios::binary);
        if (!m_model.ParseFromIstream(&input_file))
        {
            panic("Failed to parse neural network model.");
        }
    }
};

void RunOpenglStyleTransfer() {
    WindowAppConfig winConfig = {
        ivec2(1024,512), 
        "net test",
        ExecutionTypes::LimitedFrames(60),
        {
            WindowBoolHint(WindowBoolSetting::RESIZABLE, false),
            WindowBoolHint(WindowBoolSetting::VISIBLE, true)
        }
    };
    StyleTransferAppConfig styleTransferConfig = {
        unord_map<string, string>({
            {"engine", MORPH_ENGINE_RES},
            {"shared", MORPH_SHARED_RES}
        })
    };

    StyleTransferScene::CameraData& cameraData = styleTransferConfig.sceneData.cameraData;
    StyleTransferScene::ObjectData& objectDat = styleTransferConfig.sceneData.objectData;
    Light& light = styleTransferConfig.sceneData.light;
    Material& material = styleTransferConfig.sceneData.material;

    cameraData.transform.pos = vec3(-0.375, 0, 0.792);
    cameraData.transform.rotAngle = -0.425f;
    cameraData.transform.rotAxis = vec3(0, 1, 0);

    objectDat.transform.scale = vec3(0.5);

    light.lightType = LightType::POINT;
    light.attenuationLin = 2;
    light.attenuationQuad = 3;

    material.metallic = 0.4;
    material.roughness = 0.6;

    styleTransferConfig.sceneData.sideBySide = true;

    OpenglStyleTransferApp app(winConfig, styleTransferConfig);
    app.Run();
}

}

int main(int, char**) {
    Morph::RunOpenglStyleTransfer();
}
