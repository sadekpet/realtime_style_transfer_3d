#ifndef MORPH_NEURAL_NET_RESOURCES_HPP
#define MORPH_NEURAL_NET_RESOURCES_HPP

#include <Core/Core.hpp>
#include <Graphics/Context.hpp>
#include <Graphics/Uniforms.hpp>
#include <Resource/GraphicsProgramCompiler.hpp>
#include <NeuralNet/LayerProgram.hpp>

namespace Morph
{

struct Conv2dID
{
    u32 in_channels;
    u32 out_channels;
    u32 kernel_size_x;
    u32 kernel_size_y;
    u32 stride_x;
    u32 stride_y;
    bool use_bias;
};


class NeuralNetResources
{
private:
    ref<GraphicsProgramCompiler> m_progCompiler;
    opt<LayerProgram> m_inputProgram;
    opt<LayerProgram> m_outputProgram;
    opt<LayerProgram> m_addProgram;
    opt<LayerProgram> m_cat2Program;
    opt<LayerProgram> m_cat3Program;
    opt<LayerProgram> m_leakyReluProgram;
    opt<LayerProgram> m_reluProgram;
    opt<LayerProgram> m_tanhProgram;
    opt<LayerProgram> m_upsamplingProgram;
    map<u32, LayerProgram> m_batchNorm2dPrograms;
    map<tuple<u32, u32, u32, u32, u32, u32, bool>, LayerProgram> m_Conv2dPrograms;
public:
    NeuralNetResources(GraphicsProgramCompiler& progCompiler)
        : m_progCompiler(progCompiler)
    {}

    inline LayerProgram& GetInputProgram() { return GetOrCompileAndGet(m_inputProgram, "Input"); }
    inline LayerProgram& GetOutputProgram() { return GetOrCompileAndGet(m_outputProgram, "Output"); }
    inline LayerProgram& GetAddProgram() { return GetOrCompileAndGet(m_addProgram, "Add"); }
    inline LayerProgram& GetCat2Program() { return GetOrCompileAndGet(m_cat2Program, "Cat2"); }
    inline LayerProgram& GetCat3Program() { return GetOrCompileAndGet(m_cat3Program, "Cat3"); }
    inline LayerProgram& GetLeakyReluProgram() { return GetOrCompileAndGet(m_leakyReluProgram, "LeakyReLU"); }
    inline LayerProgram& GetReluProgram() { return GetOrCompileAndGet(m_reluProgram, "ReLU"); }
    inline LayerProgram& GetTanhProgram() { return GetOrCompileAndGet(m_tanhProgram, "Tanh"); }
    inline LayerProgram& GetUpsamplingProgram() { return GetOrCompileAndGet(m_upsamplingProgram, "Upsampling"); }
    LayerProgram& GetBatchNorm2dProgram(u32 num_features, uvec3 groupSize = uvec3(16,16,1));
    LayerProgram& GetConv2dProgram(Conv2dID id);
private:
    LayerProgram& GetOrCompileAndGet(opt<LayerProgram>& opt_var, string name, uvec3 groupSize = uvec3(16,16,1));
};

}

#endif // MORPH_NEURAL_NET_RESOURCES_HPP