#include "LeakyReLU.hpp"

#include <NeuralNet/Utils.hpp>

namespace Morph
{

LeakyReLUProgram::LeakyReLUProgram(NeuralNetResources& resources, const style_transfer::LeakyReLU& leakyReLU)
    :
    m_uniforms(unique<LeakyReLUUniforms>(new LeakyReLUUniforms())),
    m_negative_slopeUniform("u_negative_slope", m_uniforms->negative_slope),
    m_inputUnitUniform("u_input_data", m_uniforms->inputUnit),
    m_outputUnitUniform("u_output_data", m_uniforms->outputUnit),
    m_program(resources.GetLeakyReluProgram())
{
    m_uniforms->negative_slope = leakyReLU.negative_slope();
}

void LeakyReLUProgram::Evaluate(GraphicsContext& context, Texture3D& inputData, Texture3D& outputData)
{
    uvec3 dim = divide_upper(inputData.dim(), m_program.get().groupSize);
    ComputeProgramBinder programBinder = context.BindProgram(m_program.get().program);
    m_negative_slopeUniform.Set(programBinder);
    m_inputUnitUniform.Set(programBinder);
    m_outputUnitUniform.Set(programBinder);
    ImageBinder<true, false> inputBinder = context.BindImage<true, false>(m_uniforms->inputUnit, inputData, true);
    ImageBinder<false, true> ouputBinder = context.BindImage<false, true>(m_uniforms->outputUnit, outputData, true);
    context.DispatchCompute(programBinder, dim);
}
    
}