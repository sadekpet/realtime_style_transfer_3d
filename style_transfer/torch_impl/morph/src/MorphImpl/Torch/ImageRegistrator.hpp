#ifndef STYLE_TRANSFER_MORPH_IMPL_IMAGE_REGISTRATOR_HPP
#define STYLE_TRANSFER_MORPH_IMPL_IMAGE_REGISTRATOR_HPP

#include <Graphics/Texture.hpp>

#include "TorchFuncs.hpp"

namespace Morph { namespace MorphImpl {

class ImageRegistrator
{
private:
    TorchFuncs* m_torchFuncs = nullptr;
    void* m_cudaImageResource = nullptr;
public:
    ImageRegistrator(TorchFuncs& torchFuncs, Texture2D& texture);
    virtual ~ImageRegistrator();
    inline void*& cudaResource() { return m_cudaImageResource; }
};

}}


#endif // STYLE_TRANSFER_MORPH_IMPL_IMAGE_REGISTRATOR_HPP