#include "ImageRegistrator.hpp"

namespace Morph { namespace MorphImpl {

ImageRegistrator::ImageRegistrator(TorchFuncs& torchFuncs, Texture2D& texture)
{
    m_torchFuncs = &torchFuncs;
    m_torchFuncs->RegisterImage(
        m_cudaImageResource, 
        texture.id(), 
        GraphicsEnums::textureTypes.ToValue(TextureType::_2D)
    );
}

ImageRegistrator::~ImageRegistrator()
{
    m_torchFuncs->UnregisterImage(m_cudaImageResource);
}

}}