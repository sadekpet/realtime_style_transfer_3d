#ifndef STYLE_TRANSFER_TORCH_IMPL_HPP
#define STYLE_TRANSFER_TORCH_IMPL_HPP

#include <string>

namespace Morph { namespace TorchImpl {

void* CreateTorchData(unsigned int width, unsigned int height, std::string model);

void DeleteTorchData(void* torchData);

void RegisterImage(void*& out_resource, unsigned int texture_id, unsigned int texture_type);

void UnregisterImage(void* resource);

void InvertImage(void* torchData, void*& fromImageResource, void*& toImageResource);

void EvalImage(void* torchData, void*& fromImageResource, void*& toImageResource);

void EvalPNGImage(std::string model, std::string in_location, std::string out_location);

void* CudaEventCreate();

void CudaEventDestroy(void* event);

void CudaEventRecord(void* event);

void CudaEventSynchronize(void* event);

bool CudaEventElapsedTime(float* ms, void* start_event, void* end_event);

bool CudaEventQuery(void* event);

void CudaDeviceSynchronize();

}}

#endif // STYLE_TRANSFER_TORCH_IMPL_HPP