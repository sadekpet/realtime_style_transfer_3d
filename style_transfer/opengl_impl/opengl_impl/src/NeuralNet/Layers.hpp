#ifndef MORPH_NEURAL_NET_LAYERS_HPP
#define MORPH_NEURAL_NET_LAYERS_HPP

#include <Core/Core.hpp>

#include "Layers/Add.hpp"
#include "Layers/BatchNorm2d.hpp"
#include "Layers/Cat2.hpp"
#include "Layers/Cat3.hpp"
#include "Layers/Conv2d.hpp"
#include "Layers/Input.hpp"
#include "Layers/LeakyReLU.hpp"
#include "Layers/Output.hpp"
#include "Layers/ReLU.hpp"
#include "Layers/Tanh.hpp"
#include "Layers/Upsampling.hpp"

namespace Morph
{
    using NeuralNetLayerVar = variant<
    AddProgram,
    BatchNorm2dProgram,
    Cat2Program,
    Cat3Program,
    Conv2dProgram,
    LeakyReLUProgram,
    ReLUProgram,
    TanhProgram,
    UpsamplingProgram
    >;
}

#endif // MORPH_NEURAL_NET_LAYERS_HPP