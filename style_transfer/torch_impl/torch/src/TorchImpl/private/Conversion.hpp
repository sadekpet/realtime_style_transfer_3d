#ifndef STYLE_TRANSFER_TORCH_IMPL_CONVERSION_HPP
#define STYLE_TRANSFER_TORCH_IMPL_CONVERSION_HPP

#include <cuda_runtime.h>
#include <torch/torch.h>

namespace Morph { namespace TorchImpl {


void ConvertSurfaceToTensor(cudaSurfaceObject_t surface, float* tensor, int dimx, int dimy);

void ConvertTensorToSurface(cudaSurfaceObject_t surface, torch::PackedTensorAccessor64<float, 3> tensor);

}}

#endif // STYLE_TRANSFER_TORCH_IMPL_CONVERSION_HPP