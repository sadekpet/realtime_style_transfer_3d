#include "CudaEventTimer.hpp"

namespace Morph { namespace MorphImpl {

CudaEventTimer::CudaEventTimer(TorchFuncs& torchFuncs)
    : m_torchFuncs(&torchFuncs)
{
    m_startEvent = m_torchFuncs->CudaEventCreate();
    m_endEvent = m_torchFuncs->CudaEventCreate();
}
CudaEventTimer::CudaEventTimer(CudaEventTimer&& old)
{
    std::swap(m_torchFuncs, old.m_torchFuncs);
    std::swap(m_startEvent, old.m_startEvent);
    std::swap(m_endEvent, old.m_endEvent);
}
CudaEventTimer& CudaEventTimer::operator=(CudaEventTimer&& old)
{
    std::swap(m_torchFuncs, old.m_torchFuncs);
    std::swap(m_startEvent, old.m_startEvent);
    std::swap(m_endEvent, old.m_endEvent);
    return *this;
}
CudaEventTimer::~CudaEventTimer()
{
    if(m_startEvent != nullptr) {
        m_torchFuncs->CudaEventDestroy(m_startEvent);
        m_torchFuncs->CudaEventDestroy(m_endEvent);
    }
}

void CudaEventTimer::Synchronize() const
{
    m_torchFuncs->CudaEventSynchronize(m_endEvent);
}
bool CudaEventTimer::Query() const
{
    return m_torchFuncs->CudaEventQuery(m_endEvent);
}
opt<float> CudaEventTimer::ElapsedTime() const
{
    float time = 0;
    bool is_available = m_torchFuncs->CudaEventElapsedTime(&time, m_startEvent, m_endEvent);
    return is_available ? opt<float>(time) : opt<float>();
}
float CudaEventTimer::ElapsedTimeBlocking() const
{
    Synchronize();
    return ElapsedTime().value_or(0);
}


CudaEventTimerScope::CudaEventTimerScope(TorchFuncs& torchFuncs, CudaEventTimer& timer)
    : m_torchFuncs(&torchFuncs), m_timer(&timer)
{
    m_torchFuncs->CudaEventRecord(m_timer->start());
}
CudaEventTimerScope::CudaEventTimerScope(CudaEventTimerScope&& old)
{
    std::swap(m_torchFuncs, old.m_torchFuncs);
    std::swap(m_timer, old.m_timer);
}
CudaEventTimerScope& CudaEventTimerScope::operator=(CudaEventTimerScope&& old)
{
    std::swap(m_torchFuncs, old.m_torchFuncs);
    std::swap(m_timer, old.m_timer);
    return *this;
}
CudaEventTimerScope::~CudaEventTimerScope()
{
    if(m_timer != nullptr) {
        m_torchFuncs->CudaEventRecord(m_timer->end());
    }
}

}}