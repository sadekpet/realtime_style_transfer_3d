#include "GlobalState.hpp"

using namespace Morph;

TestsGlobalState* TestsGlobalState::s_state = nullptr;

TestsGlobalState::TestsGlobalState()
    : 
    m_windowHints({WindowBoolHint(WindowBoolSetting::VISIBLE, false)}),
    m_windowManager(WindowManager::Get()),
    m_windowManagerErrorAttacher(m_windowManager.get(), this, &TestsGlobalState::OnWindowManagerError),
    m_windowID(value_or_panic(m_windowManager->CreateWindow(ivec2(512,512), "test window", m_windowHints), "failed to create window")),
    m_window(value_or_panic(m_windowManager->GetWindow(m_windowID), "window unexpectedly destroyed")),
    m_context(value_or_panic(GraphicsContext::MakeCurrent(m_window))),
    m_resStorage(unord_map<string, string>({{"engine", MORPH_ENGINE_RES}, {"shared", MORPH_SHARED_RES}})),
    m_progCompiler(*m_context, m_resStorage),
    m_neuralNetResources(m_progCompiler)
{
}

void TestsGlobalState::Init()
{
    
    if(s_state) {
        delete s_state;
    }
    s_state = new TestsGlobalState();
}
void TestsGlobalState::Release()
{
    if(s_state) {
        delete s_state;
    }
}

Morph::Window& TestsGlobalState::window()
{
    if(!s_state) {
        s_state = new TestsGlobalState();
    }
    return s_state->m_window;
}
Morph::GraphicsContext& TestsGlobalState::context()
{
    if(!s_state) {
        s_state = new TestsGlobalState();
    }
    return *(s_state->m_context);
}
Morph::NeuralNetResources& TestsGlobalState::neuralNetResources()
{
    if(!s_state) {
        s_state = new TestsGlobalState();
    }
    return s_state->m_neuralNetResources;
}

void TestsGlobalState::OnWindowManagerError(const WindowManagerError& error) {
    spdlog::error("Window manager error {}:{}: {}", error.type, enum_integer(error.type), error.message);
}