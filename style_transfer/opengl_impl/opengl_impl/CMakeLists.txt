
file(GLOB_RECURSE OPENGL_IMPL_SRC
"src/**.h"
"src/**.hpp"
"src/**.cpp"
"src/**.cc"
)

set(OPENGL_IMPL_INCLUDE 
"src" 
"../../../extern/morphengine/engine/src" 
"../../../extern/morphengine/extern/glm" 
"../../../extern/morphengine/extern/stb"
"../../../extern/morphengine/extern/fmt/include"
"../../../extern/morphengine/extern/spdlog/include"
"../../../extern/protobuf/src"
)

add_library(opengl_impl STATIC ${OPENGL_IMPL_SRC})

target_include_directories(opengl_impl PUBLIC ${OPENGL_IMPL_INCLUDE})

target_link_libraries(opengl_impl PUBLIC morph_engine)
target_link_libraries(opengl_impl PUBLIC libprotobuf)


