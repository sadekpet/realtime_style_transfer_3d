#include "Resources.hpp"

namespace Morph
{

LayerProgram& NeuralNetResources::GetOrCompileAndGet(opt<LayerProgram>& opt_var, string name, uvec3 groupSize)
{ 
    if(!opt_var)
    {
        m_progCompiler.get().SetDefines({
            GraphicsProgramDefine("GROUP_SIZE_X", (int)groupSize.x),
            GraphicsProgramDefine("GROUP_SIZE_Y", (int)groupSize.y),
            GraphicsProgramDefine("GROUP_SIZE_Z", (int)groupSize.z)
        });
        opt_var = { groupSize, value_or_panic(m_progCompiler.get().CompileComputeProgramRes(
            "shared:opengl_impl/shaders/neuralnet/"+name+".glsl"
        ))};
        m_progCompiler.get().SetDefines({});
    }
    return opt_var.value(); 
}

LayerProgram& NeuralNetResources::GetConv2dProgram(Conv2dID id)
{
    tuple<u32, u32, u32, u32, u32, u32, bool> key = 
        {id.in_channels, id.out_channels, id.kernel_size_x, id.kernel_size_y, id.stride_x, id.stride_y, id.use_bias};
    auto mapIt = m_Conv2dPrograms.find(key);
    if(mapIt == m_Conv2dPrograms.end()) {
        uvec3 groupSize = uvec3(8, 8, 1);
        m_progCompiler.get().SetDefines({
            GraphicsProgramDefine("GROUP_SIZE_X", (int)groupSize.x),
            GraphicsProgramDefine("GROUP_SIZE_Y", (int)groupSize.y),
            GraphicsProgramDefine("GROUP_SIZE_Z", (int)groupSize.z),
            GraphicsProgramDefine("IN_CHANNELS", (int)id.in_channels),
            GraphicsProgramDefine("OUT_CHANNELS", (int)id.out_channels),
            GraphicsProgramDefine("KERNEL_SIZE_X", (int)id.kernel_size_x),
            GraphicsProgramDefine("KERNEL_SIZE_Y", (int)id.kernel_size_y),
            GraphicsProgramDefine("STRIDE_X", (int)id.stride_x),
            GraphicsProgramDefine("STRIDE_Y", (int)id.stride_y),
            GraphicsProgramDefine("USE_BIAS", (int)id.use_bias)
        });
        m_Conv2dPrograms[key] = {groupSize, value_or_panic(m_progCompiler.get().CompileComputeProgramRes(
            "shared:opengl_impl/shaders/neuralnet/Conv2d.glsl"
        ))};
        m_progCompiler.get().SetDefines({});

        return m_Conv2dPrograms[key];
    } else {
        return mapIt->second;
    }
}


LayerProgram& NeuralNetResources::GetBatchNorm2dProgram(u32 num_features, uvec3 groupSize)
{
    auto mapIt = m_batchNorm2dPrograms.find(num_features);
    if(mapIt == m_batchNorm2dPrograms.end()) {
        m_progCompiler.get().SetDefines({
            GraphicsProgramDefine("GROUP_SIZE_X", (int)groupSize.x),
            GraphicsProgramDefine("GROUP_SIZE_Y", (int)groupSize.y),
            GraphicsProgramDefine("GROUP_SIZE_Z", (int)groupSize.z),
            GraphicsProgramDefine("NUM_FEATURES", (int)num_features)
        });
        m_batchNorm2dPrograms[num_features] = {groupSize, value_or_panic(m_progCompiler.get().CompileComputeProgramRes(
            "shared:opengl_impl/shaders/neuralnet/BatchNorm2d.glsl"
        ))};
        m_progCompiler.get().SetDefines({});

        return m_batchNorm2dPrograms[num_features];
    } else {
        return mapIt->second;
    }
}

}