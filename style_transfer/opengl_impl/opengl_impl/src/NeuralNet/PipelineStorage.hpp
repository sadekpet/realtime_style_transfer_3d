#ifndef MORPH_NEURAL_NET_PIPELINE_STORAGE_HPP
#define MORPH_NEURAL_NET_PIPELINE_STORAGE_HPP

#include <Core/Core.hpp>
#include <Graphics/Context.hpp>

namespace Morph
{

class NeuralNetPipelineStorage
{
    using Slot = size_t;
    using LayerIndex = size_t;
    using Shape = std::tuple<uint, uint, uint>;
    struct SlotUsage
    {
        shared<bool> used;
        Slot slot;
    };
private:
    TextureSettings m_textureSettings = {
        TextureMinFilterSetting(TextureMinFilter::NEAREST),
        TextureMagFilterSetting(TextureMagFilter::NEAREST),
        TextureWrap2DSetting(TextureWrapType::CLAMP_TO_BORDER)
    };
    ref<GraphicsContext> m_context;
    vector<Texture3D> m_storageSlots;
    vector<Slot> m_layerSlots;
public:
    NeuralNetPipelineStorage(GraphicsContext& context) : m_context(context) {}

    void Create(vector<uvec3> dims, vector<vector<LayerIndex>> dependencies);

    const Texture3D& Get(LayerIndex layerIndex) const { return m_storageSlots[m_layerSlots[layerIndex]]; }
    const Texture3D& operator[](LayerIndex layerIndex) const { return Get(layerIndex); }

    Texture3D& Get(LayerIndex layerIndex) { return m_storageSlots[m_layerSlots[layerIndex]]; }
    Texture3D& operator[](LayerIndex layerIndex) { return Get(layerIndex); }

    const vector<Texture3D>& GetTextures() const { return m_storageSlots; }

private:
    void AssignSlotToLayer(LayerIndex layerIndex, uvec3 slotDim, map<Shape, vector<SlotUsage>>& shapeToSlotUsages, vector<shared<bool>>& layersUsingSlots);
    Slot AddStorageSlot(uvec3 dim);
    Shape DimToShape(uvec3 dim) const;
};

}

#endif // MORPH_NEURAL_NET_PIPELINE_STORAGE_HPP