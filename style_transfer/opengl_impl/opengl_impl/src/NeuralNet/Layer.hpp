#ifndef MORPH_NEURAL_NET_LAYERS_HPP
#define MORPH_NEURAL_NET_LAYERS_HPP

#include <Core/Core.hpp>

namespace Morph 
{

class LayerProgram
{
public:
    virtual uvec3 GetResultDim(uve3 input_dim) const = 0;
};

}

#endif // MORPH_NEURAL_NET_LAYERS_HPP