#include <StyleTransferApp.hpp>

namespace Morph {

void RunDemoApp() {
    WindowAppConfig winConfig = {
        ivec2(512,512), 
        "demo app",
        ExecutionTypes::LimitedFrames(60),
        {
            WindowBoolHint(WindowBoolSetting::RESIZABLE, false)
        }
    };
    StyleTransferAppConfig styleTransferConfig = {
        unord_map<string, string>({
            {"engine", MORPH_ENGINE_RES},
            {"shared", MORPH_SHARED_RES}
        })
    };

    StyleTransferScene::CameraData& cameraData = styleTransferConfig.sceneData.cameraData;
    StyleTransferScene::ObjectData& objectDat = styleTransferConfig.sceneData.objectData;
    Light& light = styleTransferConfig.sceneData.light;

    cameraData.transform.pos = vec3(-1, 0.75, 2);
    cameraData.transform.rotAxis = vec3(0, 1, 0);

    objectDat.transform.scale = vec3(0.5);

    light.lightType = LightType::POINT;
    light.position = vec4(0, 0, 1, 1);
    light.attenuationLin = 2;
    light.attenuationQuad = 3;

    styleTransferConfig.sceneData.showLight = true;

    styleTransferConfig.showGUI = true;
    styleTransferConfig.lazyUpdate = false;

    StyleTransferApp app(winConfig, styleTransferConfig);
    app.Run();
}

}

int main(int, char**) {
    Morph::RunDemoApp();
}