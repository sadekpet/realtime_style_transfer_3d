#include "ModelPrinter.hpp"

#include <iostream>
#include <fstream>
#include "nndata.pb.h"

using namespace magic_enum::ostream_operators;

namespace Morph
{

void ModelPrinter::Print(string path, uint level)
{
    using namespace style_transfer;

    NeuralNet model;
    {
        std::fstream input_file(path, std::ios::in | std::ios::binary);
        if (!model.ParseFromIstream(&input_file))
        {
            std::cerr << "Failed to parse neural network model." << std::endl;
        }
    }
    std::cout << "layers count: " << model.layers_size() << std::endl;
    for (size_t i = 0; i < model.layers_size(); i++)
    {
        const Layer &layer = model.layers(i);
        Layer::LayerOneofCase layerType = layer.layer_oneof_case();
        std::cout << layer.id() << " " << layerType << std::endl;
        if (layerType == Layer::LayerOneofCase::kConv2D)
        {
            const Conv2d &conv2D = layer.conv2d();
        }
        switch (layerType)
        {
        case Layer::LayerOneofCase::kConv2D:
            break;

        default:
            break;
        }
    }
}

} // namespace Morph