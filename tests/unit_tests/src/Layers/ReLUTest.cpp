
#include <gtest/gtest.h>
#include <NeuralNet/Layers/ReLU.hpp>
#include <GlobalState.hpp>
#include <TestingUtils.hpp>

using namespace Morph;

TEST(LayersReLU, singleTexture) {
    ReLUProgram reLUProgram(TestsGlobalState::neuralNetResources());

    array<float, 8> inputValues = {
        0, 1, 100, 1000, 0, -1, -100, -1000
    };

    Texture3D inputData = TestsUtils::CreateTexture3D(uvec3(2), inputValues.data());

    reLUProgram.Evaluate(TestsGlobalState::context(), inputData, inputData);

    array<float, 8> actualOutputValues;
    array<float, 8> expectedOutputValues = {
        0, 1, 100, 1000, 0, 0, 0, 0
    };
    inputData.GetData(actualOutputValues.data());

    ASSERT_EQ(actualOutputValues, expectedOutputValues);
}

TEST(LayersReLU, doubleTexture) {
    ReLUProgram reLUProgram(TestsGlobalState::neuralNetResources());

    array<float, 8> inputValues = {
        0, 1, 100, 1000, 0, -1, -100, -1000
    };

    Texture3D inputData = TestsUtils::CreateTexture3D(uvec3(2), inputValues.data());
    Texture3D outputData = TestsUtils::CreateTexture3D(uvec3(2));

    reLUProgram.Evaluate(TestsGlobalState::context(), inputData, outputData);

    array<float, 8> actualOutputValues;
    array<float, 8> expectedOutputValues = {
        0, 1, 100, 1000, 0, 0, 0, 0
    };
    outputData.GetData(actualOutputValues.data());

    ASSERT_EQ(actualOutputValues, expectedOutputValues);
}