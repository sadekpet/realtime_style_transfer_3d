#ifndef MORPH_NEURAL_NET_LAYERS_LEAKY_RELU_HPP
#define MORPH_NEURAL_NET_LAYERS_LEAKY_RELU_HPP

#include <Core/Core.hpp>
#include <Graphics/Context.hpp>
#include <Graphics/Uniforms.hpp>

#include <NeuralNet/Resources.hpp>
#include <NeuralNet/nndata.pb.h>

namespace Morph
{

class LeakyReLUProgram
{
    struct LeakyReLUUniforms
    {
        float negative_slope = 0;
        TextureUnit inputUnit = TextureUnit::_0;
        TextureUnit outputUnit = TextureUnit::_1;
    };
private:
    unique<LeakyReLUUniforms> m_uniforms;
    Uniform<float> m_negative_slopeUniform;
    Uniform<TextureUnit> m_inputUnitUniform;
    Uniform<TextureUnit> m_outputUnitUniform;
    ref<LayerProgram> m_program;
public:
    LeakyReLUProgram(NeuralNetResources& resources, const style_transfer::LeakyReLU& leakyReLU);

    void Evaluate(GraphicsContext& context, Texture3D& inputData, Texture3D& outputData);
};

}

#endif // MORPH_NEURAL_NET_LAYERS_LEAKY_RELU_HPP