#include <gtest/gtest.h>
#include <NeuralNet/Layers/Upsampling.hpp>
#include <GlobalState.hpp>
#include <TestingUtils.hpp>

using namespace Morph;

TEST(LayersUpsampling, basicData_scale1) {
    style_transfer::Upsampling upsampling;
    upsampling.set_scale_factor(1);
    
    UpsamplingProgram upsamplingProgram(TestsGlobalState::neuralNetResources(), upsampling);

    array<float, 8> inputValues = {
        1, 2,
        3, 4,

        5, 6,
        7, 8
    };

    Texture3D inputData = TestsUtils::CreateTexture3D(uvec3(2), inputValues.data());
    Texture3D outputData = TestsUtils::CreateTexture3D(uvec3(2));

    upsamplingProgram.Evaluate(TestsGlobalState::context(), inputData, outputData);

    array<float, 8>& expectedOutputValues = inputValues;
    array<float, 8> actualOutputValues;
    outputData.GetData(actualOutputValues.data());

    ASSERT_PRED3(TestsUtils::CompareFloatsN<8>, actualOutputValues, expectedOutputValues, TestsUtils::precision());
}

TEST(LayersUpsampling, basicData_scale2) {
    style_transfer::Upsampling upsampling;
    upsampling.set_scale_factor(2);
    
    UpsamplingProgram upsamplingProgram(TestsGlobalState::neuralNetResources(), upsampling);

    array<float, 8> inputValues = {
        1, 2,
        3, 4,

        5, 6,
        7, 8
    };

    Texture3D inputData = TestsUtils::CreateTexture3D(uvec3(2), inputValues.data());
    Texture3D outputData = TestsUtils::CreateTexture3D(uvec3(4, 4, 2));

    upsamplingProgram.Evaluate(TestsGlobalState::context(), inputData, outputData);

    array<float, 32> expectedOutputValues = {
        1, 1, 2, 2,
        1, 1, 2, 2,
        3, 3, 4, 4,
        3, 3, 4, 4,
                    
        5, 5, 6, 6,
        5, 5, 6, 6,
        7, 7, 8, 8,
        7, 7, 8, 8
    };
    array<float, 32> actualOutputValues;
    outputData.GetData(actualOutputValues.data());

    ASSERT_PRED3(TestsUtils::CompareFloatsN<32>, actualOutputValues, expectedOutputValues, TestsUtils::precision());
}

TEST(LayersUpsampling, basicData_scale3) {
    style_transfer::Upsampling upsampling;
    upsampling.set_scale_factor(3);
    
    UpsamplingProgram upsamplingProgram(TestsGlobalState::neuralNetResources(), upsampling);

    array<float, 8> inputValues = {
        1, 2,
        3, 4,

        5, 6,
        7, 8
    };

    Texture3D inputData = TestsUtils::CreateTexture3D(uvec3(2), inputValues.data());
    Texture3D outputData = TestsUtils::CreateTexture3D(uvec3(6, 6, 2));

    upsamplingProgram.Evaluate(TestsGlobalState::context(), inputData, outputData);

    array<float, 72> expectedOutputValues = {
        1, 1, 1, 2, 2, 2,
        1, 1, 1, 2, 2, 2,
        1, 1, 1, 2, 2, 2,
        3, 3, 3, 4, 4, 4,
        3, 3, 3, 4, 4, 4,
        3, 3, 3, 4, 4, 4,
                                
        5, 5, 5, 6, 6, 6,
        5, 5, 5, 6, 6, 6,
        5, 5, 5, 6, 6, 6,
        7, 7, 7, 8, 8, 8,
        7, 7, 7, 8, 8, 8,
        7, 7, 7, 8, 8, 8
    };
    array<float, 72> actualOutputValues;
    outputData.GetData(actualOutputValues.data());

    ASSERT_PRED3(TestsUtils::CompareFloatsN<72>, actualOutputValues, expectedOutputValues, TestsUtils::precision());
}