#ifndef STYLE_TRANSFER_APP_HPP
#define STYLE_TRANSFER_APP_HPP

#include <App/WindowApp.hpp>
#include <Resource/ResourceManager.hpp>
#include <Resource/GraphicsProgramCompiler.hpp>
#include <Resource/Common.hpp>

#include "StyleTransfer/Scene.hpp"
#include "StyleTransfer/Control.hpp"
#include "StyleTransfer/GUI.hpp"

namespace Morph {

struct StyleTransferAppConfig
{
    unord_map<string, string> resFolders;
    StyleTransferScene::DataConfig sceneData;
    bool showGUI = false;
    bool lazyUpdate = true;
};

class StyleTransferApp : public WindowApp
{
private:
    Void d_initializationInfo;
    GraphicsSettingsApplier m_graphicsSettingsApplier;
    Void d_clearDefaultFramebuffer;
protected:
    ResourceStorage m_resStorage;
    GraphicsProgramCompiler m_progCompiler;
    CommonResources m_commonResources;
    bool m_updateTransform;
    bool m_forceUpdateTransform;
    StyleTransferScene m_scene;
    StyleTransferGUI m_gui;
    StyleTransferControl m_control;
    bool m_firtUpdate = true;
public:
    StyleTransferApp(const WindowAppConfig& winConfig, const StyleTransferAppConfig& appConfig);
protected:
    virtual void Transform();
    // time in ms
    virtual float TransformTime();

    inline ResourceStorage& resStorage() { return m_resStorage; }
    inline GraphicsProgramCompiler& progCompiler() { return m_progCompiler; }
    inline CommonResources& commonResources() { return m_commonResources; }
    inline StyleTransferScene& scene() { return m_scene; }
private:
    void RunIter(f64 lastIterTime, f64 lastFrameTime) override;

    GraphicsSettingsApplier ApplyGraphicsSettings();
    void ClearDefaultFramebuffer();
};

}


#endif // STYLE_TRANSFER_APP_HPP