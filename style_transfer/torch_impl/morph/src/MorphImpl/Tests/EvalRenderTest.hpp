#ifndef STYLE_TRANSFER_MORPH_IMPL_EVAL_RENDER_TEST_HPP
#define STYLE_TRANSFER_MORPH_IMPL_EVAL_RENDER_TEST_HPP

#include <string>

#include <MorphImpl/Torch/TorchFuncs.hpp>

namespace Morph { namespace MorphImpl {

void EvalRenderTest(TorchFuncs torchFuncs);

}}


#endif // STYLE_TRANSFER_MORPH_IMPL_EVAL_RENDER_TEST_HPP