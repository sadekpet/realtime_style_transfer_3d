#ifndef STYLE_TRANSFER_TORCH_API_HPP
#define STYLE_TRANSFER_TORCH_API_HPP

#include <MorphImpl/Torch/TorchFuncs.hpp>
#include <TorchImpl/TorchImpl.hpp>

namespace Morph {

MorphImpl::TorchFuncs GetTorchAPI() {
    using namespace TorchImpl;
    return {
        &CreateTorchData,
        &DeleteTorchData,
        &RegisterImage,
        &UnregisterImage,
        &InvertImage,
        &EvalImage,
        &EvalPNGImage,
        &CudaEventCreate,
        &CudaEventDestroy,
        &CudaEventRecord,
        &CudaEventSynchronize,
        &CudaEventElapsedTime,
        &CudaEventQuery,
        &CudaDeviceSynchronize
    };
}

}

#endif // STYLE_TRANSFER_TORCH_API_HPP