#ifndef MORPH_NEURAL_NET_LAYERS_CONV2D_HPP
#define MORPH_NEURAL_NET_LAYERS_CONV2D_HPP

#include <Core/Core.hpp>
#include <Graphics/Context.hpp>
#include <Graphics/Uniforms.hpp>

#include <NeuralNet/Resources.hpp>
#include <NeuralNet/nndata.pb.h>

namespace Morph
{

class Conv2dProgram
{
    struct Conv2dUniforms
    {
        TextureUnit inputUnit = TextureUnit::_0;
        TextureUnit outputUnit = TextureUnit::_1;
    };
private:
    unique<Conv2dUniforms> m_uniforms;
    Conv2dID m_conv2dID;
    GraphicsBuffer<GraphicsBufferTarget::SHADER_STORAGE,0,0,0> m_weightsBuffer;
    UniformBuffer<0,0,0> m_biasesBuffer;
    Uniform<TextureUnit> m_inputUnitUniform;
    Uniform<TextureUnit> m_outputUnitUniform;
    ref<LayerProgram> m_program;
public:
    Conv2dProgram(GraphicsContext& context, NeuralNetResources& resources, const style_transfer::Conv2d& conv2d, bool transpose = true);

    void Evaluate(GraphicsContext& context, Texture3D& inputData, Texture3D& outputData);
};

}

#endif // MORPH_NEURAL_NET_LAYERS_CONV2D_HPP