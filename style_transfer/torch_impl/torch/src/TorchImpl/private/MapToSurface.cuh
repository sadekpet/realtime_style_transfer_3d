#ifndef STYLE_TRANSFER_TORCH_IMPL_MAP_TO_SURFACE_HPP
#define STYLE_TRANSFER_TORCH_IMPL_MAP_TO_SURFACE_HPP

#include <cuda_runtime.h>

#include "CudaDebug.hpp"

namespace Morph { namespace TorchImpl {

class MapToSurface
{
    cudaGraphicsResource** m_imageResource;
    cudaArray_t m_writeArray;
    cudaResourceDesc m_wdsc;
    cudaSurfaceObject_t m_writeSurface;
public:
    MapToSurface(cudaGraphicsResource** imageResource)
        : m_imageResource(imageResource)
    {
        CUERR(cudaGraphicsMapResources(1, m_imageResource));
        CUERR(cudaGraphicsSubResourceGetMappedArray(&m_writeArray, *m_imageResource, 0, 0));
        m_wdsc.resType = cudaResourceTypeArray;
        m_wdsc.res.array.array = m_writeArray;
        CUERR(cudaCreateSurfaceObject(&m_writeSurface, &m_wdsc));
    }
    ~MapToSurface()
    {
        CUERR(cudaDestroySurfaceObject(m_writeSurface));
        CUERR(cudaGraphicsUnmapResources(1, m_imageResource));
        //CUERR(cudaStreamSynchronize(0));
    }

    cudaSurfaceObject_t surface() { return m_writeSurface; }
};

}}

#endif // STYLE_TRANSFER_TORCH_IMPL_MAP_TO_SURFACE_HPP