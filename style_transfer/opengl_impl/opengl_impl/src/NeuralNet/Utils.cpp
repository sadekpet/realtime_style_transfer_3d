#include "Utils.hpp"

using namespace style_transfer;

namespace Morph
{

uint divide_upper(uint a, uint d)
{
    return d == 1 ? a : (a + d - 1) / d;
}

uvec3 divide_upper(uvec3 a, uvec3 d)
{
    return uvec3(divide_upper(a.x, d.x), divide_upper(a.y, d.y), divide_upper(a.z, d.z));
}

vector3d<float> ComputeConv2d(const Conv2d& conv2d, const vector3d<float>& inputValues)
{

    craw<float> biases(conv2d.out_channels(), conv2d.biases().data());
    craw4d<float> weights(
        uvec4(conv2d.kernel_size_x(), conv2d.kernel_size_y(), conv2d.in_channels(), conv2d.out_channels()), 
        conv2d.weights().data()
    );

    uvec3 outputDim = uvec3(inputValues.dim().x / conv2d.stride_x(), inputValues.dim().y / conv2d.stride_y(), conv2d.out_channels());
    vector3d<float> outputValues(outputDim, 0);

    ivec2 kernel_shift = ivec2(conv2d.kernel_size_x()/2, conv2d.kernel_size_y()/2);
    ivec2 stride = ivec2(conv2d.stride_x(), conv2d.stride_y());
    uvec2 pos = ivec2(0, 0);
    for(usize out_channel = 0; out_channel < conv2d.out_channels(); ++out_channel) {
        for(pos.y = 0; pos.y < outputDim.y; ++pos.y) {
            for(pos.x = 0; pos.x < outputDim.x; ++pos.x) {
                uvec3 out_pos = uvec3(pos.x, pos.y, out_channel);
                float& out_value = outputValues[out_pos];
                out_value = conv2d.use_bias() ? biases[out_channel] : 0;
                for(usize in_channel = 0; in_channel < conv2d.in_channels(); ++in_channel) {
                    ivec2 offset = ivec2(0, 0);
                    for(offset.y = 0; offset.y < conv2d.kernel_size_y(); ++offset.y) {
                        for(offset.x = 0; offset.x < conv2d.kernel_size_x(); ++offset.x) {
                            ivec2 in_pos = ivec2(pos) * stride + offset - kernel_shift;
                            if(in_pos.x >= 0 && in_pos.y >= 0 && in_pos.x < inputValues.dim().x && in_pos.y < inputValues.dim().y) {
                                out_value += 
                                    inputValues[uvec3(in_pos.x, in_pos.y, in_channel)] *
                                    weights[uvec4(offset.x, offset.y, in_channel, out_channel)];
                            }
                        }
                    }
                }
            }
        }
    }
    return outputValues;
}

}