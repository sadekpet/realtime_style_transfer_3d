

file(GLOB_RECURSE TESTS_SRC
    "src/**.h"
    "src/**.hpp"
    "src/**.cpp"
    "src/**.cc"
)

set(TESTS_INCLUDE 
    "src" 
    "../../style_transfer/opengl_impl/opengl_impl/src"
    "../../extern/morphengine/engine/src"
    "../../extern/morphengine/extern/glm"
    "../../extern/morphengine/extern/googletest/googletest/include"
)

add_executable(style_transfer_lib_unit_tests ${TESTS_SRC})

target_include_directories(style_transfer_lib_unit_tests PUBLIC ${TESTS_INCLUDE})

target_link_libraries(style_transfer_lib_unit_tests PRIVATE glm)
target_link_libraries(style_transfer_lib_unit_tests PRIVATE gtest gmock)
target_link_libraries(style_transfer_lib_unit_tests PRIVATE opengl_impl)

add_test(style_transfer_lib_unit_tests style_transfer_lib_unit_tests)
