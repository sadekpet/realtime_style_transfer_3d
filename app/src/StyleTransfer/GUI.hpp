#ifndef STYLE_TRANSFER_APP_GUI_HPP
#define STYLE_TRANSFER_APP_GUI_HPP

#include <ImGui/ImGui.hpp>

#include "Scene.hpp"

namespace Morph {

class StyleTransferGUI
{
private:
    bool m_show;
    ImGuiContext m_imguiContext;
    ref<StyleTransferScene> m_scene;
    bool m_valuesChanged = false;
    bool m_noUpdates = false;
    bool m_lazyUpdate;
public:
    StyleTransferGUI(bool show, Window& window, StyleTransferScene& scene, bool lazyUpdate);

    void Display(float transformTime);

    inline bool& show() { return m_show; }
    inline bool& valuesChanged() { return m_valuesChanged; }
    inline bool& noUpdates() { return m_noUpdates; }
    inline bool& lazyUpdate() { return m_lazyUpdate; }

private:
    void Changed(bool changed);
};

}


#endif // STYLE_TRANSFER_APP_GUI_HPP