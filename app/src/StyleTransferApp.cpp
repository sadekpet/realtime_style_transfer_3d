#include "StyleTransferApp.hpp"

namespace Morph {

StyleTransferApp::StyleTransferApp(const WindowAppConfig& winConfig, const StyleTransferAppConfig& appConfig)
    : WindowApp(winConfig),
    d_initializationInfo(MORPH_VOID(spdlog::info("initialization started"))),
    m_graphicsSettingsApplier(ApplyGraphicsSettings()),
    d_clearDefaultFramebuffer(MORPH_VOID(ClearDefaultFramebuffer())),
    m_resStorage(appConfig.resFolders),
    m_progCompiler(context(), m_resStorage),
    m_commonResources(context(), m_resStorage, m_progCompiler),
    m_updateTransform(true),
    m_forceUpdateTransform(false),
    m_scene(context(), m_resStorage, m_commonResources, appConfig.sceneData),
    m_gui(appConfig.showGUI, window(), m_scene, appConfig.lazyUpdate),
    m_control(windowManager(), window(), m_scene, m_gui, m_updateTransform, m_forceUpdateTransform)
{
}

void StyleTransferApp::Transform()
{
    // just copies from inTexture to outTexture
    FramebufferBinder framebufferBinder = context().BindFramebuffer(m_scene.outFramebuffer());
    framebufferBinder.Clear();
    RenderProgramBinder programBinder = context().BindProgram(m_commonResources.ScreenFillProgram());
    TextureUnitBinder textureUnitBinder = context().BindTextureUnit(
        m_scene.textureSamplerUnit(),
        m_scene.inTexture()
    );
    m_scene.textureSamplerUniform().Set(programBinder);
    m_commonResources.ScreenQuad2DMesh().Draw(context(), framebufferBinder, programBinder);
}

float StyleTransferApp::TransformTime()
{
    return 0;
}

void StyleTransferApp::RunIter(f64 lastIterTime, f64 lastFrameTime)
{
    if(m_firtUpdate) {
        spdlog::info("initialization finished");
        m_firtUpdate = false;
    }
    m_control.Update(lastIterTime, lastFrameTime);
    m_scene.Render();
    if(((m_updateTransform || !m_gui.lazyUpdate()) && !m_gui.noUpdates()) || m_forceUpdateTransform) {
        m_scene.PreTransform();
        Transform();
        m_updateTransform = false;
        m_forceUpdateTransform = false;
    }
    m_scene.Display();
    m_gui.Display(TransformTime());
}


GraphicsSettingsApplier StyleTransferApp::ApplyGraphicsSettings()
{
    GraphicsSettings graphicsSettings = {
        MultisampleEnabledSetting(true),
        DepthTestEnabledSetting(true),
        CullFaceEnabledSetting(true),
        CullFaceSetting(CullFaceType::BACK),
        FrontFaceSetting(FrontFaceType::CCW),
        ClearColorSetting(0)
    };
    return context().ApplySettings(graphicsSettings);
}

void StyleTransferApp::ClearDefaultFramebuffer()
{
    DefaultFramebuffer& defaultFramebuffer = context().GetDefaultFramebuffer();
    FramebufferBinder framebufferBinder = context().BindFramebuffer(defaultFramebuffer);
    framebufferBinder.Clear();
    defaultFramebuffer.SwapBuffers();
    windowManager().PollEvents();
}

}