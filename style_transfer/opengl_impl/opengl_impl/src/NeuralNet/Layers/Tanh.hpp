#ifndef MORPH_NEURAL_NET_LAYERS_TANH_HPP
#define MORPH_NEURAL_NET_LAYERS_TANH_HPP

#include <Core/Core.hpp>
#include <Graphics/Context.hpp>
#include <Graphics/Uniforms.hpp>

#include <NeuralNet/Resources.hpp>

namespace Morph
{

class TanhProgram
{
    struct TanhUniforms
    {
        TextureUnit inputUnit = TextureUnit::_0;
        TextureUnit outputUnit = TextureUnit::_1;
    };
private:
    unique<TanhUniforms> m_uniforms;
    Uniform<TextureUnit> m_inputUnitUniform;
    Uniform<TextureUnit> m_outputUnitUniform;
    ref<LayerProgram> m_program;
public:
    TanhProgram(NeuralNetResources& resources);

    void Evaluate(GraphicsContext& context, Texture3D& inputData, Texture3D& outputData);
};

}

#endif // MORPH_NEURAL_NET_LAYERS_TANH_HPP