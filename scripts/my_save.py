import argparse
import os
import sys
import time
import re

from PIL import Image
import numpy as np
import torch
import torch.nn as nn
from torch.autograd import Variable
from torchvision import models
from torchvision import transforms

from models import GeneratorJ


if __name__ == "__main__":
    if len(sys.argv) != 3:
        print("usage: my_save.py <in_model> <out_model>")
    else:
        in_model_file = sys.argv[1]
        out_model_file = sys.argv[2]

        device = torch.device("cuda")

        with torch.no_grad():
            in_model = torch.load(in_model_file)
            example = torch.rand(1, 3, 512, 512).to(device)
            traced_script_module = torch.jit.trace(in_model, example)
            traced_script_module.save(out_model_file)
 
