#include "Input.hpp"

#include <NeuralNet/Utils.hpp>

namespace Morph
{

NeuralNetInputProgram::NeuralNetInputProgram(NeuralNetResources& resources, vec3 mean, vec3 std)
    : 
    m_uniforms(unique<InputUniforms>(new InputUniforms())),
    m_meanUniform("u_mean", m_uniforms->mean),
    m_stdUniform("u_std", m_uniforms->std),
    m_inputUnitUniform("u_input_data", m_uniforms->inputUnit),
    m_outputUnitUniform("u_output_data", m_uniforms->outputUnit),
    m_program(resources.GetInputProgram())
{
    m_uniforms->mean = mean;
    m_uniforms->std = std;
}

void NeuralNetInputProgram::Evaluate(GraphicsContext& context, Texture2D& inputTexture, Texture3D& outputData)
{
    uvec3 dim = uvec3(
        divide_upper(inputTexture.dim().x, m_program.get().groupSize.x), 
        divide_upper(inputTexture.dim().y, m_program.get().groupSize.y), 
        1);
    ComputeProgramBinder programBinder = context.BindProgram(m_program.get().program);
    m_meanUniform.Set(programBinder);
    m_stdUniform.Set(programBinder);
    m_inputUnitUniform.Set(programBinder);
    m_outputUnitUniform.Set(programBinder);
    ImageBinder<true, false> inputBinder = context.BindImage<true, false>(m_uniforms->inputUnit, inputTexture);
    ImageBinder<false, true> ouputBinder = context.BindImage<false, true>(m_uniforms->outputUnit, outputData, true);
    context.DispatchCompute(programBinder, dim);
}

}