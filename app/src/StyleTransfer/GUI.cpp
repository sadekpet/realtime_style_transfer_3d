#include "GUI.hpp"

namespace Morph {

StyleTransferGUI::StyleTransferGUI(bool show, Window& window, StyleTransferScene& scene, bool lazyUpdate)
    : m_show(show),
    m_imguiContext(window),
    m_scene(scene),
    m_lazyUpdate(lazyUpdate)
{

}

void StyleTransferGUI::Display(float transformTime)
{
    m_valuesChanged = false;
    if(m_show)
    {
        float renderTime = (float)m_scene->renderTimer().GetAvgTime() / 1000000.0f;
        float displayTime = (float)m_scene->displayTimer().GetAvgTime() / 1000000.0f;

        ImGuiDraw imguiDraw = m_imguiContext.Draw();

        ImGui::Begin("style transfer settings");

        Changed(ImGui::Checkbox("lazy update", &m_lazyUpdate));
        Changed(ImGui::Checkbox("no updates", &m_noUpdates));
        ImGui::Checkbox("mask output", &m_scene->maskOutput());
        {
            static const char* items[] = {
                "STYLE", "NORMALS", "LIGHT_DIR", "MASK"
            };
            ImGui::Combo("output type", &m_scene->outputTypeUnsafe(), items, IM_ARRAYSIZE(items));
        }
        {
            static const char* items[] = {
                "BUNNY", "SPHERE", "CUBE"
            };
            Changed(ImGui::Combo("model type", &m_scene->modelTypeUnsafe(), items, IM_ARRAYSIZE(items)));
        }
        if (ImGui::CollapsingHeader("Camera"))
        {
            Changed(ImGui::DragFloat3("position##1", (float*)&m_scene->cameraData().transform.pos, 0.025));
            Changed(ImGui::DragFloat("rotation", &m_scene->cameraData().transform.rotAngle, 0.005));
        }
        if (ImGui::CollapsingHeader("Light"))
        {
            Changed(ImGui::Checkbox("show", &m_scene->showLight()));
            Changed(ImGui::DragFloat3("position##2", (float*)&m_scene->light().position, 0.01));
            Changed(ImGui::ColorEdit3("color", (float*)&m_scene->light().color));
            Changed(ImGui::DragFloat3("attenuation", (float*)&m_scene->light().attenuationLin, 0.005, 0, 100));
        }
        if (ImGui::CollapsingHeader("Material"))
        {
            Changed(ImGui::ColorEdit3("albedo", (float*)&m_scene->material().albedo));
            Changed(ImGui::DragFloat("metallic", &m_scene->material().metallic, 0.01, 0, 1));
            Changed(ImGui::DragFloat("roughness", &m_scene->material().roughness, 0.01, 0, 1));
            Changed(ImGui::DragFloat("ao", &m_scene->material().ao, 0.01, 0, 1));
        }
        if (ImGui::CollapsingHeader("Statistics", ImGuiTreeNodeFlags_DefaultOpen))
        {
            ImGui::Text("transform %.3f ms", transformTime);
            ImGui::Text("render scene %.3f ms", renderTime);
            ImGui::Text("display %.3f ms", displayTime);
            ImGui::Text("application frame %.3f ms (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
        }

        ImGui::End();
    }
}

void StyleTransferGUI::Changed(bool changed)
{
    if(changed) {
        m_valuesChanged = true;
    }
}

}