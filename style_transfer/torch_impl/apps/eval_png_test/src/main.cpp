
#include <TorchImpl/TorchImpl.hpp>

int main() {
    
    using namespace Morph::TorchImpl;

    EvalPNGImage(
        std::string(STYLE_TRANSFER_CMAKE_ROOT) + "/scripts/res/to_red.pt", 
        std::string(STYLE_TRANSFER_CMAKE_ROOT) + "/scripts/res/sphere_white.png",
        std::string("out.png")
    );
}