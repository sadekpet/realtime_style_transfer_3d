#ifndef STYLE_TRANSFER_TORCH_IMPL_PNG_HPP
#define STYLE_TRANSFER_TORCH_IMPL_PNG_HPP

#include <string>
#include <torch/torch.h>

namespace Morph { namespace TorchImpl {

torch::Tensor ReadTensorFrom_PNG(std::string filename);

void WriteTensorTo_PNG(const torch::Tensor& texture, std::string filename);

}}

#endif // STYLE_TRANSFER_TORCH_IMPL_PNG_HPP