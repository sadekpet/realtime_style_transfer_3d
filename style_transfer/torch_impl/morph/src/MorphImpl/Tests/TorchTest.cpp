#include "TorchTest.hpp"

#include <StyleTransferApp.hpp>
#include <Profile/GraphicsTimer.hpp>
#include <MorphImpl/Torch/TorchData.hpp>
#include <MorphImpl/Torch/CudaTimer.hpp>

namespace Morph { namespace MorphImpl {

class TorchTestApp : public StyleTransferApp
{
private:
    MorphImpl::TorchFuncs m_torchFuncs;
    // torch data operations
    ImageRegistrator m_fromImageRegistrator;
    ImageRegistrator m_toImageRegistrator;
    std::string m_modelPath;
    TorchData m_torchData;
    // timers
    CudaTimer<10> m_cudaTimer;
public:
    TorchTestApp(const WindowAppConfig& winConfig, const StyleTransferAppConfig& appConfig, MorphImpl::TorchFuncs torchFuncs)
        : StyleTransferApp(winConfig, appConfig),
        m_torchFuncs(torchFuncs),
        m_fromImageRegistrator(m_torchFuncs, scene().inTexture()),
        m_toImageRegistrator(m_torchFuncs, scene().outTexture()),
        m_modelPath(std::string(STYLE_TRANSFER_CMAKE_ROOT) + "/scripts/res/to_red.pt"),
        m_torchData(m_torchFuncs, uvec2(scene().inTexture().dim().x, scene().inTexture().dim().y), m_modelPath),
        m_cudaTimer(m_torchFuncs)
    {
        scene().modelType() = StyleTransferScene::ModelType::SPHERE;
    }
protected:
    void Transform() override
    {
        {
            auto cudaTimerScope = m_cudaTimer.Begin();
            m_torchData.InvertImage(m_fromImageRegistrator, m_toImageRegistrator);
        }
    }

    float TransformTime() override
    {
        return m_cudaTimer.GetAvgTime();
    }
};

void TorchTest(TorchFuncs torchFuncs) {
    WindowAppConfig winConfig = {
        ivec2(1024,512), 
        "cuda draw test",
        ExecutionTypes::LimitedFrames(60),
        {
            WindowBoolHint(WindowBoolSetting::RESIZABLE, false)
        }
    };
    StyleTransferAppConfig styleTransferConfig = {
        unord_map<string, string>({
            {"engine", MORPH_ENGINE_RES},
            {"shared", MORPH_SHARED_RES}
        })
    };

    StyleTransferScene::CameraData& cameraData = styleTransferConfig.sceneData.cameraData;
    StyleTransferScene::ObjectData& objectDat = styleTransferConfig.sceneData.objectData;
    Light& light = styleTransferConfig.sceneData.light;
    Material& material = styleTransferConfig.sceneData.material;

    cameraData.transform.pos = vec3(-0.375, 0, 0.792);
    cameraData.transform.rotAngle = -0.425f;
    cameraData.transform.rotAxis = vec3(0, 1, 0);

    objectDat.transform.scale = vec3(0.5);

    light.lightType = LightType::POINT;
    light.attenuationLin = 2;
    light.attenuationQuad = 3;

    material.metallic = 0.4;
    material.roughness = 0.6;

    styleTransferConfig.sceneData.sideBySide = true;
    styleTransferConfig.lazyUpdate = false;

    TorchTestApp app(winConfig, styleTransferConfig, torchFuncs);
    app.Run();
}

}}

