#version 450 core

layout(local_size_x = GROUP_SIZE_X, local_size_y = GROUP_SIZE_Y, local_size_z = GROUP_SIZE_Z) in;

readonly layout(r32f) uniform image3D u_input_data1;
readonly layout(r32f) uniform image3D u_input_data2;
writeonly layout(r32f) uniform image3D u_output_data;

void main() {
    ivec3 coord = ivec3(gl_GlobalInvocationID);
    float value1 = imageLoad(u_input_data1, coord).x;
    float value2 = imageLoad(u_input_data2, coord).x;
    float res = value1 + value2;
    imageStore(u_output_data, coord, vec4(res));
}