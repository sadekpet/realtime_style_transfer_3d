#ifndef STYLE_TRANSFER_APP_SCENE_HPP
#define STYLE_TRANSFER_APP_SCENE_HPP

#include <Graphics/Context.hpp>
#include <Resource/Common.hpp>
#include <Data/ShaderTransform.hpp>
#include <Data/Transform.hpp>
#include <Data/Camera.hpp>
#include <Data/PBR.hpp>
#include <Profile/GraphicsTimer.hpp>

namespace Morph {

class StyleTransferScene
{
public:
    struct CameraData
    {
        PerspectiveCamera3D camera;
        Transform transform;
    };

    struct ObjectData
    {
        Transform transform;
    };

    struct DataConfig
    {
        CameraData cameraData;
        ObjectData objectData;
        Light light;
        Material material;
        bool showLight = false;
        vec3 envGlobalAmbient = vec3(0.001);
        bool sideBySide = false;
    };

    enum class OutputType : int {
        STYLE = 0,
        NORMALS,
        LIGHT_DIR,
        MASK
    };
    enum class ModelType : int {
        BUNNY = 0,
        SPHERE,
        CUBE
    };
private:
    struct TextureFramebuffer {
        CustomFramebuffer framebuffer;
        Texture2D texture;
        Renderbuffer renderbuffer;

        TextureFramebuffer(GraphicsContext& context, uvec2 dim);
    };
private:
    ref<GraphicsContext> m_context;
    ref<ResourceStorage> m_resStorage;
    ref<CommonResources> m_commonResources;
    // scene data
    CameraData m_cameraData;
    ObjectData m_objectData;
    Light m_light;
    Material m_material;
    bool m_showLight;
    vec3 m_envGlobalAmbient;
    uint m_lightCount = 1;
    MaterialTex m_textures;
    ShaderTransform m_shaderTransform;
    raw<Light> m_rawLights;
    // uniforms
    ShaderTransformUniforms m_shaderTransformUniforms;
    PBRForwardUniforms m_pbrForwardUniforms;
    PBRForwardTextureUnits m_pbrForwardTextureUnits;
    Uniform<vec3> m_lightColorUniform;
    TextureUnit m_textureSamplerUnit;
    Uniform<TextureUnit> m_textureSamplerUniform;
    TextureUnit m_maskSamplerUnit;
    Uniform<TextureUnit> m_maskSamplerUniform;
    // framebuffers
    ref<DefaultFramebuffer> m_defaultFramebuffer;
    // texture dim
    bool m_sideBySide;
    uvec2 m_textureDim;
    // texture framebuffers
    TextureFramebuffer m_in;
    TextureFramebuffer m_out;
    TextureFramebuffer m_normals;
    TextureFramebuffer m_lightDir;
    TextureFramebuffer m_mask;
    // meshes
    ref<IndexedStaticMesh3D<u32>> m_sphere;
    ref<IndexedStaticMesh3D<u32>> m_cube;
    IndexedStaticMesh3D<u32> m_bunny;
    // timers
    GraphicsTimer<5> m_renderTimer;
    GraphicsTimer<5> m_displayTimer;
    // what to show on output
    OutputType m_outputType;
    // model to chose
    ModelType m_modelType;
    // mask output
    bool m_maskOutput;
public:
    StyleTransferScene(GraphicsContext& context, ResourceStorage& resStorage, CommonResources& commonResources, const DataConfig& config);
    void Render();
    void PreTransform();
    void Display();

    inline bool& showLight() { return m_showLight; }

    inline CameraData& cameraData() { return m_cameraData; }
    inline ObjectData& objectData() { return m_objectData; }
    inline Light& light() { return m_light; }
    inline Material& material() { return m_material; }
    inline ShaderTransform& shaderTransform() { return m_shaderTransform; }

    inline TextureUnit& textureSamplerUnit() { return m_textureSamplerUnit; }
    inline Uniform<TextureUnit>& textureSamplerUniform() { return m_textureSamplerUniform; }

    inline Texture2D& inTexture() { return m_in.texture; }
    inline Texture2D& outTexture() { return m_out.texture; }
    inline Texture2D& normalsTexture() { return m_normals.texture; }
    inline Texture2D& lightDirTexture() { return m_lightDir.texture; }
    inline Texture2D& maskTexture() { return m_mask.texture; }
    inline DefaultFramebuffer& defaultFramebuffer() { return m_defaultFramebuffer; }
    inline CustomFramebuffer& outFramebuffer() { return m_out.framebuffer; }

    inline GraphicsTimer<5>& renderTimer() { return m_renderTimer; }
    inline GraphicsTimer<5>& displayTimer() { return m_displayTimer; }

    inline OutputType& outputType() { return m_outputType; }
    inline ModelType& modelType() { return m_modelType; }
    inline int& outputTypeUnsafe() { return reinterpret_cast<int&>(m_outputType); }
    inline int& modelTypeUnsafe() { return reinterpret_cast<int&>(m_modelType); }
    inline bool& maskOutput() { return m_maskOutput; }

    float aspectRatio() const;
private:
    void DisplayTexture(const FramebufferBinder& framebufferBinder, Texture& texture, uvec2 offset, uvec2 size, bool mask);
    
    void Render(TextureFramebuffer& textureFramebuffer, RenderProgram& program);

    Texture2D& OutputTypeToTexture();

    IndexedStaticMesh3D<u32>& mesh();
};  

}


#endif // STYLE_TRANSFER_APP_SCENE_HPP