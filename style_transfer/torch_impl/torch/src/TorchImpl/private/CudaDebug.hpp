#ifndef STYLE_TRANSFER_TORCH_IMPL_CUDA_DEBUG_HPP
#define STYLE_TRANSFER_TORCH_IMPL_CUDA_DEBUG_HPP

#include <cuda_runtime.h>

#include <iostream>

#ifdef MORPH_WINDOWS
    #define MORPH_DEBUG_BREAK __debugbreak()
#elif MORPH_LINUX
    #include <signal.h>
    #define MORPH_DEBUG_BREAK raise(SIGTRAP)
#else
    #define MORPH_DEBUG_BREAK 0
#endif

#ifndef MORPH_DEBUG
#define CUERR(action) action
#else // ifdef MORPH_DEBUG
#define CUERR(action) if(Morph::TorchImpl::CudaError(action, __FILE__, __LINE__)) {MORPH_DEBUG_BREAK;}
#endif // MORPH_DEBUG

namespace Morph { namespace TorchImpl {

bool CudaError(cudaError err, const char* file, int line)
{
    bool res = false;
    if(err != cudaError::cudaSuccess) {
        res = true;
        std::cout << "Cuda error " << err << ", line " << line << ", file " << file << std::endl;
    }
    return res;
}

}}

#endif // STYLE_TRANSFER_TORCH_IMPL_CUDA_DEBUG_HPP