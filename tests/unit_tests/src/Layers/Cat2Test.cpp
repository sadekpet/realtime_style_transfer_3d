#include <gtest/gtest.h>
#include <NeuralNet/Layers/Cat2.hpp>
#include <GlobalState.hpp>
#include <TestingUtils.hpp>

using namespace Morph;

TEST(LayersCat2, basicData_i1_i1) {
    style_transfer::Cat2 cat2;
    cat2.set_id1(0);
    cat2.set_id2(0);
    
    Cat2Program cat2Program(TestsGlobalState::neuralNetResources(), cat2);

    array<float, 4> inputValues1 = {
        1, 2,
        3, 4
    };
    array<float, 4> inputValues2 = {
        5, 6,
        7, 8
    };

    Texture3D inputData1 = TestsUtils::CreateTexture3D(uvec3(2,2,1), inputValues1.data());
    Texture3D inputData2 = TestsUtils::CreateTexture3D(uvec3(2,2,1), inputValues2.data());
    Texture3D outputData = TestsUtils::CreateTexture3D(uvec3(2,2,2));

    cat2Program.Evaluate(TestsGlobalState::context(), inputData1, inputData2, outputData);

    array<float, 8> expectedOutputValues = {
        1, 2,
        3, 4,

        5, 6,
        7, 8
    };
    array<float, 8> actualOutputValues;
    outputData.GetData(actualOutputValues.data());

    ASSERT_PRED3(TestsUtils::CompareFloatsN<8>, actualOutputValues, expectedOutputValues, TestsUtils::precision());
}

TEST(LayersCat2, basicData_i2_i1) {
    style_transfer::Cat2 cat2;
    cat2.set_id1(0);
    cat2.set_id2(0);
    
    Cat2Program cat2Program(TestsGlobalState::neuralNetResources(), cat2);

    array<float, 8> inputValues1 = {
        1, 2,
        3, 4,

        5, 6,
        7, 8
    };
    array<float, 4> inputValues2 = {
         9, 10,
        11, 12
    };

    Texture3D inputData1 = TestsUtils::CreateTexture3D(uvec3(2,2,2), inputValues1.data());
    Texture3D inputData2 = TestsUtils::CreateTexture3D(uvec3(2,2,1), inputValues2.data());
    Texture3D outputData = TestsUtils::CreateTexture3D(uvec3(2,2,3));

    cat2Program.Evaluate(TestsGlobalState::context(), inputData1, inputData2, outputData);

    array<float, 12> expectedOutputValues = {
        1, 2,
        3, 4,

        5, 6,
        7, 8,

         9, 10,
        11, 12
    };
    array<float, 12> actualOutputValues;
    outputData.GetData(actualOutputValues.data());

    ASSERT_PRED3(TestsUtils::CompareFloatsN<12>, actualOutputValues, expectedOutputValues, TestsUtils::precision());
}

TEST(LayersCat2, basicData_i1_i2) {
    style_transfer::Cat2 cat2;
    cat2.set_id1(0);
    cat2.set_id2(0);
    
    Cat2Program cat2Program(TestsGlobalState::neuralNetResources(), cat2);

    array<float, 4> inputValues1 = {
        1, 2,
        3, 4
    };
    array<float, 8> inputValues2 = {
        5, 6,
        7, 8,
        
         9, 10,
        11, 12
    };

    Texture3D inputData1 = TestsUtils::CreateTexture3D(uvec3(2,2,1), inputValues1.data());
    Texture3D inputData2 = TestsUtils::CreateTexture3D(uvec3(2,2,2), inputValues2.data());
    Texture3D outputData = TestsUtils::CreateTexture3D(uvec3(2,2,3));

    cat2Program.Evaluate(TestsGlobalState::context(), inputData1, inputData2, outputData);

    array<float, 12> expectedOutputValues = {
        1, 2,
        3, 4,

        5, 6,
        7, 8,

         9, 10,
        11, 12
    };
    array<float, 12> actualOutputValues;
    outputData.GetData(actualOutputValues.data());

    ASSERT_PRED3(TestsUtils::CompareFloatsN<12>, actualOutputValues, expectedOutputValues, TestsUtils::precision());
}