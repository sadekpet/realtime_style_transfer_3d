#include "TestingUtils.hpp"

using namespace Morph;
using namespace style_transfer;

Morph::Texture2D TestsUtils::CreateTexture2D(Morph::uvec2 dim)
{
    TextureSettings textureSettings = {
        TextureMinFilterSetting(TextureMinFilter::NEAREST),
        TextureMagFilterSetting(TextureMagFilter::NEAREST),
        TextureWrap2DSetting(TextureWrapType::CLAMP_TO_BORDER)
    };
    return TestsGlobalState::context().CreateTexture2D(dim, TextureSizedFormat::RGBA8, textureSettings);
}
Morph::Texture2D TestsUtils::CreateTexture2D(Morph::uvec2 dim, const void* data)
{
    TextureSettings textureSettings = {
        TextureMinFilterSetting(TextureMinFilter::NEAREST),
        TextureMagFilterSetting(TextureMagFilter::NEAREST),
        TextureWrap2DSetting(TextureWrapType::CLAMP_TO_BORDER)
    };
    return TestsGlobalState::context().CreateTexture2D(dim, data, TextureSizedFormat::RGBA8, textureSettings);
}

Morph::Texture3D TestsUtils::CreateTexture3D(Morph::uvec3 dim)
{
    TextureSettings textureSettings = {
        TextureMinFilterSetting(TextureMinFilter::NEAREST),
        TextureMagFilterSetting(TextureMagFilter::NEAREST),
        TextureWrap2DSetting(TextureWrapType::CLAMP_TO_BORDER)
    };
    return TestsGlobalState::context().CreateTexture3D(dim, TextureSizedFormat::R32F, textureSettings);
}
Morph::Texture3D TestsUtils::CreateTexture3D(Morph::uvec3 dim, const void* data)
{
    TextureSettings textureSettings = {
        TextureMinFilterSetting(TextureMinFilter::NEAREST),
        TextureMagFilterSetting(TextureMagFilter::NEAREST),
        TextureWrap2DSetting(TextureWrapType::CLAMP_TO_BORDER)
    };
    return TestsGlobalState::context().CreateTexture3D(dim, data, TextureSizedFormat::R32F, textureSettings);
}

Conv2d TestsUtils::CretateConv2d(uint in_channels, uint out_channels, uvec2 kernel_size, vector<float> weights, vector<float> biases, uvec2 stride, bool use_bias)
{
    Conv2d conv2d;
    conv2d.set_in_channels(in_channels);
    conv2d.set_out_channels(out_channels);
    conv2d.set_kernel_size_x(kernel_size.x);
    conv2d.set_kernel_size_y(kernel_size.y);
    conv2d.set_stride_x(stride.x);
    conv2d.set_stride_y(stride.y);
    conv2d.set_use_bias(use_bias);
    google::protobuf::RepeatedField<float>& conv_weights = *conv2d.mutable_weights();
    conv_weights.Resize(conv2d.in_channels() * conv2d.out_channels() * conv2d.kernel_size_x() * conv2d.kernel_size_y(), 0);
    std::copy(weights.begin(), weights.end(), conv_weights.begin());
    if(use_bias) {
        google::protobuf::RepeatedField<float>& conv_biases = *conv2d.mutable_biases();
        conv_biases.Resize(conv2d.out_channels(), 0);
        std::copy(biases.begin(), biases.end(), conv_biases.begin());
    }
    return conv2d;
}
Conv2d TestsUtils::CretateUniformConv2d(uint in_channels, uint out_channels, uvec2 kernel_size, uvec2 stride, bool use_bias, float weights, float biases)
{
    Conv2d conv2d;
    conv2d.set_in_channels(in_channels);
    conv2d.set_out_channels(out_channels);
    conv2d.set_kernel_size_x(kernel_size.x);
    conv2d.set_kernel_size_y(kernel_size.y);
    conv2d.set_stride_x(stride.x);
    conv2d.set_stride_y(stride.y);
    conv2d.set_use_bias(use_bias);
    google::protobuf::RepeatedField<float>& conv_weights = *conv2d.mutable_weights();
    conv_weights.Resize(conv2d.in_channels() * conv2d.out_channels() * conv2d.kernel_size_x() * conv2d.kernel_size_y(), weights);
    if(use_bias) {
        google::protobuf::RepeatedField<float>& conv_biases = *conv2d.mutable_biases();
        conv_biases.Resize(conv2d.out_channels(), biases);
    }
    return conv2d;
}

Conv2d TestsUtils::CretateRandomConv2d(uint in_channels, uint out_channels, uvec2 kernel_size, uvec2 stride, bool use_bias)
{
    Conv2d conv2d;
    conv2d.set_in_channels(in_channels);
    conv2d.set_out_channels(out_channels);
    conv2d.set_kernel_size_x(kernel_size.x);
    conv2d.set_kernel_size_y(kernel_size.y);
    conv2d.set_stride_x(stride.x);
    conv2d.set_stride_y(stride.y);
    conv2d.set_use_bias(use_bias);
    google::protobuf::RepeatedField<float>& conv_weights = *conv2d.mutable_weights();
    conv_weights.Resize(conv2d.in_channels() * conv2d.out_channels() * conv2d.kernel_size_x() * conv2d.kernel_size_y(), 0);
    TestsUtils::GenRandFloats(conv_weights.begin(), conv_weights.end());
    if(use_bias) {
        google::protobuf::RepeatedField<float>& conv_biases = *conv2d.mutable_biases();
        conv_biases.Resize(conv2d.out_channels(), 0);
        TestsUtils::GenRandFloats(conv_biases.begin(), conv_biases.end());
    }
    return conv2d;
}