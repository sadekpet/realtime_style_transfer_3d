#include "TorchData.hpp"

namespace Morph { namespace MorphImpl {

TorchData::TorchData(TorchFuncs& torchFuncs, uvec2 dim, string modelLocation)
{
    m_torchFuncs = &torchFuncs;
    m_torchData = m_torchFuncs->CreateTorchData(dim.x, dim.y, modelLocation);
}

TorchData::TorchData(TorchData&& old)
{
    std::swap(m_torchFuncs, old.m_torchFuncs);
    std::swap(m_torchData, old.m_torchData);
}
TorchData& TorchData::operator=(TorchData&& old)
{
    std::swap(m_torchFuncs, old.m_torchFuncs);
    std::swap(m_torchData, old.m_torchData);
    return *this;
}
TorchData::~TorchData()
{
    if(m_torchData != nullptr) {
        m_torchFuncs->DeleteTorchData(m_torchData);
    }
}

void TorchData::InvertImage(ImageRegistrator& fromImage, ImageRegistrator& toImage)
{
    m_torchFuncs->InvertImage(m_torchData, fromImage.cudaResource(), toImage.cudaResource());
}
void TorchData::EvalImage(ImageRegistrator& fromImage, ImageRegistrator& toImage)
{
    m_torchFuncs->EvalImage(m_torchData, fromImage.cudaResource(), toImage.cudaResource());
}

}}