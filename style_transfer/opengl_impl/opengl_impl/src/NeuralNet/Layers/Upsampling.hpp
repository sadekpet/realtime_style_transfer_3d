#ifndef MORPH_NEURAL_NET_LAYERS_UPSAMPLING_HPP
#define MORPH_NEURAL_NET_LAYERS_UPSAMPLING_HPP

#include <Core/Core.hpp>
#include <Graphics/Context.hpp>
#include <Graphics/Uniforms.hpp>

#include <NeuralNet/Resources.hpp>
#include <NeuralNet/nndata.pb.h>

namespace Morph
{

class UpsamplingProgram
{
    struct UpsamplingUniforms
    {
        u32 scale_factor = 1;
        TextureUnit inputUnit = TextureUnit::_0;
        TextureUnit outputUnit = TextureUnit::_1;
    };
private:
    unique<UpsamplingUniforms> m_uniforms;
    Uniform<u32> m_scale_factorUniform;
    Uniform<TextureUnit> m_inputUnitUniform;
    Uniform<TextureUnit> m_outputUnitUniform;
    ref<LayerProgram> m_program;
public:
    UpsamplingProgram(NeuralNetResources& resources, const style_transfer::Upsampling& upsampling);

    void Evaluate(GraphicsContext& context, Texture3D& inputData, Texture3D& outputData);
};

}

#endif // MORPH_NEURAL_NET_LAYERS_UPSAMPLING_HPP