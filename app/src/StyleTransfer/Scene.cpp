#include "Scene.hpp"

#include <Resource/ResourceManager.hpp>

namespace Morph {


StyleTransferScene::StyleTransferScene(GraphicsContext& context, ResourceStorage& resStorage, CommonResources& commonResources, const DataConfig& config)
    : m_context(context),
    m_resStorage(resStorage),
    m_commonResources(commonResources),
    m_cameraData(config.cameraData),
    m_objectData(config.objectData),
    m_light(config.light),
    m_material(config.material),
    m_showLight(config.showLight),
    m_envGlobalAmbient(config.envGlobalAmbient),
    m_rawLights(1, &m_light),
    m_shaderTransformUniforms(m_shaderTransform),
    m_pbrForwardUniforms(m_envGlobalAmbient, m_cameraData.transform.pos, m_material, m_lightCount, m_rawLights),
    m_pbrForwardTextureUnits(m_textures, m_commonResources->WhiteTexture2D()),
    m_lightColorUniform("u_color", m_light.color),
    m_textureSamplerUnit(TextureUnit::_0),
    m_textureSamplerUniform("u_textureSampler", m_textureSamplerUnit),
    m_maskSamplerUnit(TextureUnit::_1),
    m_maskSamplerUniform("u_maskSampler", m_maskSamplerUnit),
    m_defaultFramebuffer(context.GetDefaultFramebuffer()),
    m_sideBySide(config.sideBySide),
    m_textureDim(uvec2(
        m_defaultFramebuffer->dim().x / (config.sideBySide ? 2 : 1),
        m_defaultFramebuffer->dim().y
    )),
    m_in(context, m_textureDim),
    m_out(context, m_textureDim),
    m_normals(context, m_textureDim),
    m_lightDir(context, m_textureDim),
    m_mask(context, m_textureDim),
    m_sphere(m_commonResources->SphereMesh()),
    m_cube(m_commonResources->CubeMesh()),
    m_bunny(context, 
        value_or_panic(ResourceManager::LoadMesh3D_OBJ(
        value_or_panic(resStorage.GetPath("shared:app/bunny.obj"), 
        "resource path shared:app/bunny.obj does not exist")), 
        "failed to open file shared:app/bunny.obj")
    ),
    m_renderTimer(m_context),
    m_displayTimer(m_context),
    m_outputType(OutputType::STYLE),
    m_modelType(ModelType::BUNNY),
    m_maskOutput(false)
{
}

float StyleTransferScene::aspectRatio() const
{
    return (float) m_textureDim.x / (float) m_textureDim.y;
}

void StyleTransferScene::Render()
{
    GraphicsQueryScope<GraphicsQueryType::TIME_ELAPSED> timeQueryScope = m_renderTimer.Begin();

    m_context->SetViewport(m_textureDim);
    {
        Render(m_in, m_commonResources->PBRForwardProgram());
        if(m_showLight) 
        {
            FramebufferBinder framebufferBinder = m_context->BindFramebuffer(m_in.framebuffer);
            Transform lightTrans;
            lightTrans.pos = m_light.position;
            lightTrans.scale = vec3(0.05);
            mat4 V = m_cameraData.transform.ToInv();
            mat4 P = m_cameraData.camera.ToMat(aspectRatio());
            m_shaderTransform.Set(lightTrans.ToMat(), V, P);
            RenderProgramBinder programBinder = m_context->BindProgram(m_commonResources->MeshFillProgram());
            m_shaderTransformUniforms.SetUniforms(programBinder);
            m_lightColorUniform.Set(programBinder);
            m_commonResources->CubeMesh().Draw(*m_context, framebufferBinder, programBinder);
            m_shaderTransform.Set(objectData().transform.ToMat(), V, P);
        }
    }
    m_context->SetViewport(defaultFramebuffer().dim());
}

void StyleTransferScene::PreTransform()
{
    m_context->SetViewport(m_textureDim);
    Render(m_normals, m_commonResources->MeshNormalsProgram());
    Render(m_lightDir, m_commonResources->MeshLightDirProgram());
    Render(m_mask, m_commonResources->MeshMaskProgram());
    m_context->SetViewport(defaultFramebuffer().dim());
}

void StyleTransferScene::Display()
{
    GraphicsQueryScope<GraphicsQueryType::TIME_ELAPSED> timeQueryScope = m_displayTimer.Begin();
    FramebufferBinder framebufferBinder = m_context->BindFramebuffer(m_defaultFramebuffer);
    framebufferBinder.Clear();
    if(!m_sideBySide) {
        DisplayTexture(framebufferBinder, OutputTypeToTexture(), uvec2(0), m_textureDim, m_maskOutput);
    } else {
        DisplayTexture(framebufferBinder, m_in.texture, uvec2(0), m_textureDim, false);
        DisplayTexture(framebufferBinder, OutputTypeToTexture(), uvec2(m_textureDim.x, 0), m_textureDim, m_maskOutput);
    }
    m_context->SetViewport(defaultFramebuffer().dim());
}

void StyleTransferScene::DisplayTexture(const FramebufferBinder& framebufferBinder, Texture& texture, uvec2 offset, uvec2 size, bool mask)
{
    m_context->SetViewport(offset, size);
    if(mask) {
        RenderProgramBinder programBinder = m_context->BindProgram(m_commonResources->ApplyMaskProgram());
        TextureUnitBinder textureUnitBinder = m_context->BindTextureUnit(m_textureSamplerUnit, texture);
        TextureUnitBinder maskUnitBinder = m_context->BindTextureUnit(m_maskSamplerUnit, m_mask.texture);
        m_textureSamplerUniform.Set(programBinder);
        m_maskSamplerUniform.Set(programBinder);
        m_commonResources->ScreenQuad2DMesh().Draw(m_context, framebufferBinder, programBinder);
    } else {
        RenderProgramBinder programBinder = m_context->BindProgram(m_commonResources->ScreenFillProgram());
        TextureUnitBinder textureUnitBinder = m_context->BindTextureUnit(m_textureSamplerUnit, texture);
        m_textureSamplerUniform.Set(programBinder);
        m_commonResources->ScreenQuad2DMesh().Draw(m_context, framebufferBinder, programBinder);
    }
}

void StyleTransferScene::Render(TextureFramebuffer& textureFramebuffer, RenderProgram& program)
{
    FramebufferBinder framebufferBinder = m_context->BindFramebuffer(textureFramebuffer.framebuffer);
    framebufferBinder.Clear();
    RenderProgramBinder programBinder = m_context->BindProgram(program);
    m_shaderTransformUniforms.SetUniforms(programBinder);
    m_pbrForwardUniforms.SetUniforms(programBinder);
    TextureUnitsBinder textureUnitsBinder = m_pbrForwardTextureUnits.Bind(m_context, programBinder);
    mesh().Draw(m_context, framebufferBinder, programBinder);
}

Texture2D& StyleTransferScene::OutputTypeToTexture()
{
    switch (m_outputType)
    {
    case OutputType::STYLE:
        return m_out.texture;
    case OutputType::NORMALS:
        return m_normals.texture;
    case OutputType::LIGHT_DIR:
        return m_lightDir.texture;
    case OutputType::MASK:
        return m_mask.texture;
    default:
        return m_out.texture;
    }
}

IndexedStaticMesh3D<u32>& StyleTransferScene::mesh()
{
    switch (m_modelType)
    {
    case ModelType::BUNNY:
        return m_bunny;
    case ModelType::SPHERE:
        return m_sphere;
    case ModelType::CUBE:
        return m_cube;
    default:
        return m_bunny;
    }
}

StyleTransferScene::TextureFramebuffer::TextureFramebuffer(GraphicsContext& context, uvec2 dim)
    : framebuffer(context.CreateFramebuffer()),
    texture(context.CreateTexture2D(dim, TextureSizedFormat::RGBA8,{
            TextureMinFilterSetting(TextureMinFilter::NEAREST),
            TextureMagFilterSetting(TextureMagFilter::NEAREST)
    })),
    renderbuffer(context.CreateRenderbuffer(dim, TextureSizedFormat::DEPTH24_STENCIL8))
{
    framebuffer.Attach(CustomFramebufferAttachment::COLOR_0, texture);
    framebuffer.Attach(CustomFramebufferAttachment::DEPTH_STENCIL, renderbuffer);
}

}