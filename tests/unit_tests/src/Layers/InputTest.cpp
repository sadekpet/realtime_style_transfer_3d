#include <gtest/gtest.h>
#include <NeuralNet/Layers/Input.hpp>
#include <GlobalState.hpp>
#include <TestingUtils.hpp>

using namespace Morph;

TEST(LayersInput, copyData) {
    NeuralNetInputProgram inputProgram(TestsGlobalState::neuralNetResources(), vec3(0), vec3(1));

    const int totalSize = 16;
    array<float, totalSize> inputValues = {
        0, 0, 0, 0,   0, 0, 0, 0,
        0, 0, 0, 0,   0, 0, 0, 0
    };

    Texture2D inputData = TestsUtils::CreateTexture2D(uvec2(2), inputValues.data());
    Texture3D outputData = TestsUtils::CreateTexture3D(uvec3(2, 2, 3));

    inputProgram.Evaluate(TestsGlobalState::context(), inputData, outputData);

    array<float, totalSize> actualOutputValues;
    array<float, totalSize> expectedOutputValues = {
        0, 0, 0, 0,   1, 0, 1, 1,
        0, 1, 0, 0,   1, 1, 1, 1
    };
    inputData.GetData(actualOutputValues.data());

    ASSERT_PRED3(TestsUtils::CompareFloatsN<totalSize>, actualOutputValues, expectedOutputValues, TestsUtils::precision());
}