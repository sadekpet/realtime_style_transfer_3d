#include <gtest/gtest.h>
#include <NeuralNet/Layers/BatchNorm2d.hpp>
#include <GlobalState.hpp>
#include <TestingUtils.hpp>

using namespace Morph;

TEST(LayersBatchNorm2d, doubleTexture) {
    style_transfer::BatchNorm2d batchNorm2d;
    batchNorm2d.set_num_features(2);
    batchNorm2d.set_eps(0.00001f);
    batchNorm2d.add_means(0.5f);
    batchNorm2d.add_means(0.0f);
    batchNorm2d.add_vars(0.5f);
    batchNorm2d.add_vars(1);
    batchNorm2d.add_gammas(1.0f);
    batchNorm2d.add_gammas(2.0f);
    batchNorm2d.add_betas(0.0f);
    batchNorm2d.add_betas(1.0f);
    
    BatchNorm2dProgram batchNorm2dProgram(TestsGlobalState::context(), TestsGlobalState::neuralNetResources(), batchNorm2d);

    array<float, 8> inputValues = {
        -10, 0, 1, 10, -10, 0, 1, 10
    };

    Texture3D inputData = TestsUtils::CreateTexture3D(uvec3(2), inputValues.data());
    Texture3D outputData = TestsUtils::CreateTexture3D(uvec3(2));

    batchNorm2dProgram.Evaluate(TestsGlobalState::context(), inputData, outputData);

    array<float, 8> actualOutputValues;
    array<float, 8> expectedOutputValues;

    outputData.GetData(actualOutputValues.data());
    for(size_t z = 0; z < 2; ++z) {
        for(size_t xy = 0; xy < 4; ++xy) {
            size_t i  = xy + 4 * z;
            float val = inputValues[i];
            float& res = expectedOutputValues[i];
            res = batchNorm2d.gammas(z) * (val - batchNorm2d.means(z)) /
             std::sqrt(batchNorm2d.vars(z) + batchNorm2d.eps()) + batchNorm2d.betas(z);
        }
    }

    ASSERT_PRED3(TestsUtils::CompareFloatsN<8>, actualOutputValues, expectedOutputValues, TestsUtils::precision());
}