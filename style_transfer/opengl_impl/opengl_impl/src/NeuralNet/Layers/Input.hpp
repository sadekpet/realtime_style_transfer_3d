#ifndef MORPH_NEURAL_NET_LAYERS_INPUT_HPP
#define MORPH_NEURAL_NET_LAYERS_INPUT_HPP

#include <Core/Core.hpp>
#include <Graphics/Context.hpp>
#include <Graphics/Uniforms.hpp>

#include <NeuralNet/Resources.hpp>

namespace Morph
{

class NeuralNetInputProgram
{
    struct InputUniforms
    {
        vec3 mean;
        vec3 std;
        TextureUnit inputUnit = TextureUnit::_0;
        TextureUnit outputUnit = TextureUnit::_1;
    };
private:
    unique<InputUniforms> m_uniforms;
    Uniform<vec3> m_meanUniform;
    Uniform<vec3> m_stdUniform;
    Uniform<TextureUnit> m_inputUnitUniform;
    Uniform<TextureUnit> m_outputUnitUniform;
    ref<LayerProgram> m_program;
public:
    NeuralNetInputProgram(NeuralNetResources& resources, vec3 mean = vec3(0.5f), vec3 std = vec3(0.5f));

    void Evaluate(GraphicsContext& context, Texture2D& inputTexture, Texture3D& outputData);
};

}

#endif // MORPH_NEURAL_NET_LAYERS_INPUT_HPP