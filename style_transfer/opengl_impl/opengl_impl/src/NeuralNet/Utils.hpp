#ifndef STYLE_TRANSFER_NEURAL_NET_UTILS_HPP
#define STYLE_TRANSFER_NEURAL_NET_UTILS_HPP

#include <Core/Core.hpp>
#include <NeuralNet/nndata.pb.h>

namespace Morph
{

uint divide_upper(uint a, uint d);
uvec3 divide_upper(uvec3 a, uvec3 d);
vector3d<float> ComputeConv2d(const style_transfer::Conv2d& conv2d, const vector3d<float>& inputValues);

}

#endif // STYLE_TRANSFER_NEURAL_NET_UTILS_HPP