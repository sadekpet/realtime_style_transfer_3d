#version 450 core

layout(local_size_x = GROUP_SIZE_X, local_size_y = GROUP_SIZE_Y, local_size_z = GROUP_SIZE_Z) in;

uniform uint u_dim;

readonly layout(r32f) uniform image3D u_input_data1;
readonly layout(r32f) uniform image3D u_input_data2;
writeonly layout(r32f) uniform image3D u_output_data;

void main() {
    ivec3 coord = ivec3(gl_GlobalInvocationID);
    int input_data2_offset = imageSize(u_input_data1).z;
    float res = 0;
    if(coord.z < input_data2_offset) {
        res = imageLoad(u_input_data1, coord).x;
    } else {
        res = imageLoad(u_input_data2, ivec3(coord.x, coord.y, coord.z - input_data2_offset)).x;
    }
    imageStore(u_output_data, coord, vec4(res));
}