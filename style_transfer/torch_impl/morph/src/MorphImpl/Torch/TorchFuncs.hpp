#ifndef STYLE_TRANSFER_MORPH_IMPL_TORCH_FUNCS_HPP
#define STYLE_TRANSFER_MORPH_IMPL_TORCH_FUNCS_HPP

#include <cstdlib>
#include <string>

namespace Morph { namespace MorphImpl {

struct TorchFuncs
{
    void* (*CreateTorchData)(unsigned int, unsigned int, std::string);
    void  (*DeleteTorchData)(void*);
    void  (*RegisterImage)(void*&, unsigned int, unsigned int);
    void  (*UnregisterImage)(void*);
    void  (*InvertImage)(void*, void*&, void*&);
    void  (*EvalImage)(void*, void*&, void*&);
    void  (*EvalPNGImage)(std::string, std::string, std::string);
    void* (*CudaEventCreate)();
    void  (*CudaEventDestroy)(void*);
    void  (*CudaEventRecord)(void*);
    void  (*CudaEventSynchronize)(void*);
    bool  (*CudaEventElapsedTime)(float*, void*, void*);
    bool  (*CudaEventQuery)(void*);
    void (*CudaDeviceSynchronize)();
};

}}


#endif // STYLE_TRANSFER_MORPH_IMPL_TORCH_FUNCS_HPP