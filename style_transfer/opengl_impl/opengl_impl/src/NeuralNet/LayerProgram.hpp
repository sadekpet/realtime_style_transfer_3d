#ifndef STYLE_TRANSFER_NEURAL_NET_RESOURCES_HPP
#define STYLE_TRANSFER_NEURAL_NET_RESOURCES_HPP

#include <Core/Core.hpp>
#include <Graphics/Context.hpp>

namespace Morph
{

struct LayerProgram
{
    uvec3 groupSize;
    ComputeProgram program;
};

}

#endif // STYLE_TRANSFER_NEURAL_NET_RESOURCES_HPP