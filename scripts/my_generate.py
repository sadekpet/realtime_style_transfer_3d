import argparse
import os
import sys
import time
import re

from PIL import Image
import numpy as np
import torch
import torch.nn as nn
from torch.autograd import Variable
from torchvision import models
from torchvision import transforms

from models import GeneratorJ


if __name__ == "__main__":
    if len(sys.argv) != 4:
        print("usage: my_generate.py <model> <inputfile> <outputfile>")
    else:
        model_file = sys.argv[1]
        in_file = sys.argv[2]
        out_file = sys.argv[3]

        device = torch.device("cuda")
        style_transform = transforms.Compose([
            transforms.ToTensor(),
            transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))
        ])
        in_image = Image.open(in_file).convert('RGB')
        in_data = style_transform(in_image).unsqueeze(0).to(device)
        print(in_data.dtype)
        with torch.no_grad():
            style_model = torch.load(model_file)
            out_data = style_model(in_data)
            print(out_data)
            out_data = ((out_data.clamp(-1, 1) + 1) * 127.5).permute((0, 2, 3, 1))
            out_data = out_data.cpu().data.numpy().astype(np.uint8)
            out_image = Image.fromarray(out_data[0])
            out_image.save(out_file)
