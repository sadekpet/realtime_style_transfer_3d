#include "PipelineStorage.hpp"

namespace Morph
{

void NeuralNetPipelineStorage::Create(vector<uvec3> dims, vector<vector<LayerIndex>> dependencies)
{
    m_storageSlots.clear();
    m_layerSlots.resize(dims.size());

    map<Shape, vector<SlotUsage>> shapeToSlotUsages;
    vector<shared<bool>> layersUsingSlots(dims.size());
    AssignSlotToLayer(dims.size() - 1, dims.back(), shapeToSlotUsages, layersUsingSlots);
    for(int layerIndex = dims.size() - 1; layerIndex >= 0; layerIndex--)
    {
        const vector<LayerIndex>& dep = dependencies[layerIndex];
        for(LayerIndex depLayerIndex : dep) {
            AssignSlotToLayer(depLayerIndex, dims[depLayerIndex], shapeToSlotUsages, layersUsingSlots);
        }
        if(layersUsingSlots[layerIndex]) {
            *(layersUsingSlots[layerIndex]) = false;
        } else {
            spdlog::info("neural net layer {0} has no layers depending on it", layerIndex);
        }
    }
}

void NeuralNetPipelineStorage::AssignSlotToLayer(LayerIndex layerIndex, uvec3 slotDim, map<Shape, vector<SlotUsage>>& shapeToSlotUsages, vector<shared<bool>>& layersUsingSlots)
{
    if(!layersUsingSlots[layerIndex]) {
        Shape shape = DimToShape(slotDim);
        if(shapeToSlotUsages.find(shape) == shapeToSlotUsages.end()) {
            shapeToSlotUsages.insert({shape, vector<SlotUsage>()});
        }
        vector<SlotUsage>& slotUsages = shapeToSlotUsages[shape];
        Slot slot;
        for(SlotUsage& slotUsage : slotUsages) {
            if(!*slotUsage.used) {
                *slotUsage.used = true;
                slot = slotUsage.slot;
                layersUsingSlots[layerIndex] = slotUsage.used;
            }
        }
        if(!layersUsingSlots[layerIndex]) {
            slot = AddStorageSlot(slotDim);
            slotUsages.push_back({std::make_shared<bool>(true), slot});
            layersUsingSlots[layerIndex] = slotUsages.back().used;
        }
        m_layerSlots[layerIndex] = slot;
    }
}

NeuralNetPipelineStorage::Slot NeuralNetPipelineStorage::AddStorageSlot(uvec3 dim)
{
    Slot slot = m_storageSlots.size();
    GraphicsContext& context = m_context.get();
    m_storageSlots.push_back(context.CreateTexture3D(dim, TextureSizedFormat::R32F, m_textureSettings));
    return slot;
}

NeuralNetPipelineStorage::Shape NeuralNetPipelineStorage::DimToShape(uvec3 dim) const
{
    return {dim.x, dim.y, dim.z};
}

}