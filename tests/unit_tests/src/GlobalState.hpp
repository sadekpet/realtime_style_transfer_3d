#ifndef STYLE_TRANSFER_UNIT_TESTS_GLOBAL_STATE_HPP
#define STYLE_TRANSFER_UNIT_TESTS_GLOBAL_STATE_HPP

#include <Core/Core.hpp>
#include <Window/Manager.hpp>
#include <Graphics/Context.hpp>
#include <Resource/GraphicsProgramCompiler.hpp>
#include <Resource/ResourceManager.hpp>
#include <NeuralNet/Resources.hpp>

class TestsGlobalState
{
private:
    Morph::WindowHints m_windowHints;
    Morph::unique<Morph::WindowManager> m_windowManager;
    Morph::MethodAttacher<Morph::WindowManagerError, TestsGlobalState> m_windowManagerErrorAttacher;
    Morph::WindowID m_windowID;
    Morph::ref<Morph::Window> m_window;
    Morph::unique<Morph::GraphicsContext> m_context;
    Morph::ResourceStorage m_resStorage;
    Morph::GraphicsProgramCompiler m_progCompiler;
    Morph::NeuralNetResources m_neuralNetResources;

    static TestsGlobalState* s_state;
private:
    TestsGlobalState();
public:

    static void Init();
    static void Release();

    static Morph::Window& window();
    static Morph::GraphicsContext& context();
    static Morph::NeuralNetResources& neuralNetResources();

private:
    void OnWindowManagerError(const Morph::WindowManagerError& error);
};

#endif // STYLE_TRANSFER_UNIT_TESTS_GLOBAL_STATE_HPP