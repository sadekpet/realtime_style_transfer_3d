#include <iostream>
#include <math.h>

#include <MorphImpl/Render.hpp>
#include <TorchAPI.hpp>

#include <string>

int main(int argc, char *argv[])
{
  std::string model = "";
  if(argc > 1) {
    model = argv[1];
  } else {
    //model = std::string(STYLE_TRANSFER_CMAKE_ROOT) + "/scripts/res/to_red.pt";
    model = std::string(STYLE_TRANSFER_CMAKE_ROOT) + "/scripts/res/malba7.pt";
  }

  Morph::MorphImpl::TorchStyleTransfer(model.c_str(), Morph::GetTorchAPI());

  return 0;
} 
 
