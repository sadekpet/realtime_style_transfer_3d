#include "Render.hpp"

#include <StyleTransferApp.hpp>
#include <Resource/ResourceManager.hpp>
#include <Profile/GraphicsTimer.hpp>
#include <MorphImpl/Torch/TorchData.hpp>
#include <MorphImpl/Torch/CudaTimer.hpp>

namespace Morph { namespace MorphImpl {

class TorchRenderApp : public StyleTransferApp
{
private:
    MorphImpl::TorchFuncs m_torchFuncs;
    // torch data operations
    ImageRegistrator m_fromImageRegistrator;
    ImageRegistrator m_toImageRegistrator;
    std::string m_modelPath;
    TorchData m_torchData;
    // timers
    CudaEventTimer m_cudaTimer;
    float m_cachedTime = 0;
public:
    TorchRenderApp(const WindowAppConfig& winConfig, const StyleTransferAppConfig& appConfig, MorphImpl::TorchFuncs torchFuncs, string model)
        : StyleTransferApp(winConfig, appConfig),
        m_torchFuncs(torchFuncs),
        m_fromImageRegistrator(m_torchFuncs, scene().inTexture()),
        m_toImageRegistrator(m_torchFuncs, scene().outTexture()),
        m_modelPath(model),
        m_torchData(m_torchFuncs, uvec2(scene().inTexture().dim().x, scene().inTexture().dim().y), m_modelPath),
        m_cudaTimer(m_torchFuncs)
    {
    }
protected:
    void Transform() override
    {
        CudaEventTimerScope cudaTimerScope(m_torchFuncs, m_cudaTimer);
        m_torchData.EvalImage(m_fromImageRegistrator, m_toImageRegistrator);
    }

    float TransformTime() override
    {
        m_cachedTime = m_cudaTimer.ElapsedTime().value_or(m_cachedTime);
        return m_cachedTime;
    }
};

}}

void Morph::MorphImpl::TorchStyleTransfer(const char* model, TorchFuncs torchFuncs) {
    WindowAppConfig winConfig = {
        ivec2(1024,512), 
        "torch style transfer",
        ExecutionTypes::LimitedFrames(60),
        {
            WindowBoolHint(WindowBoolSetting::RESIZABLE, false)
        }
    };
    StyleTransferAppConfig styleTransferConfig = {
        unord_map<string, string>({
            {"engine", MORPH_ENGINE_RES},
            {"shared", MORPH_SHARED_RES}
        })
    };

    StyleTransferScene::CameraData& cameraData = styleTransferConfig.sceneData.cameraData;
    StyleTransferScene::ObjectData& objectDat = styleTransferConfig.sceneData.objectData;
    Light& light = styleTransferConfig.sceneData.light;
    Material& material = styleTransferConfig.sceneData.material;

    //cameraData.transform.pos = vec3(-0.375, 0, 0.792);
    cameraData.transform.pos = vec3(0.027, 0.125, 0.709);
    //cameraData.transform.rotAngle = -0.425f;
    cameraData.transform.rotAngle = 0.194;
    cameraData.transform.rotAxis = vec3(0, 1, 0);

    objectDat.transform.scale = vec3(0.5);

    light.position = vec3(-0.260, 0.310, 0.560);
    light.lightType = LightType::POINT;
    light.attenuationLin = 2;
    light.attenuationQuad = 3;

    material.metallic = 0.4;
    material.roughness = 0.6;

    styleTransferConfig.sceneData.sideBySide = true;

    TorchRenderApp app(winConfig, styleTransferConfig, torchFuncs, model);
    app.Run();
}