
#include <gtest/gtest.h>
#include <NeuralNet/Layers/LeakyReLU.hpp>
#include <GlobalState.hpp>
#include <TestingUtils.hpp>

using namespace Morph;

TEST(LayersLeakyReLU, doubleTexture) {
    style_transfer::LeakyReLU leakyReLU;
    leakyReLU.set_negative_slope(0.1f);
    
    LeakyReLUProgram leakyReLUProgram(TestsGlobalState::neuralNetResources(), leakyReLU);

    array<float, 8> inputValues = {
        -100, -10, -5, -1, 0, 1, 5, 100
    };

    Texture3D inputData = TestsUtils::CreateTexture3D(uvec3(2), inputValues.data());
    Texture3D outputData = TestsUtils::CreateTexture3D(uvec3(2));

    leakyReLUProgram.Evaluate(TestsGlobalState::context(), inputData, outputData);

    array<float, 8> actualOutputValues;
    array<float, 8> expectedOutputValues;

    outputData.GetData(actualOutputValues.data());
    std::transform(inputValues.begin(), inputValues.end(), expectedOutputValues.begin(), 
        [&](float x) { return x > 0 ? x : x * leakyReLU.negative_slope(); }
    );

    ASSERT_PRED3(TestsUtils::CompareFloatsN<8>, actualOutputValues, expectedOutputValues, TestsUtils::precision());
}