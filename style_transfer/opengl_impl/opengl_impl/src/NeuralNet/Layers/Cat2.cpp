#include "Cat2.hpp"

#include <NeuralNet/Utils.hpp>

namespace Morph
{

Cat2Program::Cat2Program(NeuralNetResources& resources, const style_transfer::Cat2& cat2)
    : 
    m_id1(cat2.id1()), m_id2(cat2.id2()),
    m_uniforms(unique<Cat2Uniforms>(new Cat2Uniforms())),
    m_inputUnitUniform1("u_input_data1", m_uniforms->inputUnit1),
    m_inputUnitUniform2("u_input_data2", m_uniforms->inputUnit2),
    m_outputUnitUniform("u_output_data", m_uniforms->outputUnit),
    m_program(resources.GetCat2Program())
{
}

void Cat2Program::Evaluate(GraphicsContext& context, Texture3D& inputData1, Texture3D& inputData2, Texture3D& outputData)
{
    uvec3 dim = divide_upper(outputData.dim(), m_program.get().groupSize);
    ComputeProgramBinder programBinder = context.BindProgram(m_program.get().program);
    m_inputUnitUniform1.Set(programBinder);
    m_inputUnitUniform2.Set(programBinder);
    m_outputUnitUniform.Set(programBinder);
    ImageBinder<true, false> inputBinder1 = context.BindImage<true, false>(m_uniforms->inputUnit1, inputData1, true);
    ImageBinder<true, false> inputBinder2 = context.BindImage<true, false>(m_uniforms->inputUnit2, inputData2, true);
    ImageBinder<false, true> ouputBinder = context.BindImage<false, true>(m_uniforms->outputUnit, outputData, true);
    context.DispatchCompute(programBinder, dim);
}

}