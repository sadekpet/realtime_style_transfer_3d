#include "Conversion.hpp"

namespace Morph { namespace TorchImpl {

__global__
void surfaceToTensor(cudaSurfaceObject_t sur, float *arr, int dimx, int dimy)
{
  unsigned int x = blockIdx.x * blockDim.x + threadIdx.x;
  unsigned int y = blockIdx.y * blockDim.y + threadIdx.y;

  if(x < dimx && y < dimy)
  {
    uchar4 data;
    surf2Dread(&data, sur, x * sizeof(uchar4), y);
    int index = (x + y * dimx)*3;
    arr[index+0] = ((float)data.x) / 255.0f;
    arr[index+1] = ((float)data.y) / 255.0f;
    arr[index+2] = ((float)data.z) / 255.0f;
  }
}

__global__
void tensorToSurface(cudaSurfaceObject_t surface, torch::PackedTensorAccessor64<float, 3> tensor)
{
  unsigned int x = blockIdx.x * blockDim.x + threadIdx.x;
  unsigned int y = blockIdx.y * blockDim.y + threadIdx.y;

  if(x < tensor.size(0) && y < tensor.size(1))
  {
    uchar4 data = make_uchar4(
      (int)(tensor[y][x][0] * 255.0f), 
      (int)(tensor[y][x][1] * 255.0f), 
      (int)(tensor[y][x][2] * 255.0f), 
      0xff
    );
    surf2Dwrite(data, surface, x * sizeof(uchar4), y);
  }
}

void ConvertSurfaceToTensor(cudaSurfaceObject_t surface, float* tensor, int dimx, int dimy)
{
    dim3 thread(32, 32);
    dim3 block(dimx / thread.x, dimy / thread.y);
    surfaceToTensor <<< block , thread >>>(surface, tensor, dimx, dimy);
}

void ConvertTensorToSurface(cudaSurfaceObject_t surface, torch::PackedTensorAccessor64<float, 3> tensor)
{
    dim3 thread(32, 32);
    dim3 block(tensor.size(0) / thread.x, tensor.size(1) / thread.y);
    tensorToSurface <<< block , thread >>>(surface, tensor);
}

}}