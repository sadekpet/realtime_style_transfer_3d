#ifndef STYLE_TRANSFER_MORPH_IMPL_CUDA_EVENT_TIMER_HPP
#define STYLE_TRANSFER_MORPH_IMPL_CUDA_EVENT_TIMER_HPP

#include <Morph.hpp>

#include "TorchFuncs.hpp"

namespace Morph { namespace MorphImpl {

class CudaEventTimer
{
private:
    TorchFuncs* m_torchFuncs = nullptr;
    void* m_startEvent = nullptr;
    void* m_endEvent = nullptr;
public:
    CudaEventTimer() {}
    CudaEventTimer(TorchFuncs& torchFuncs);

    CudaEventTimer(const CudaEventTimer& other) = delete;
    CudaEventTimer(CudaEventTimer&& old);
    CudaEventTimer& operator=(CudaEventTimer&& old);
    virtual ~CudaEventTimer();
    inline operator bool() const { return m_startEvent != nullptr; }

    inline void* start() { return m_startEvent; }
    inline void* end() { return m_endEvent; }

    // waits until time is measured
    void Synchronize() const;
    // checks if time was measured non-blocking
    bool Query() const;
    // elapsed time in miliseconds non-blocking
    opt<float> ElapsedTime() const;
    // elapsed time in miliseconds blocking
    float ElapsedTimeBlocking() const;
};

// Scope in which to measure the time
class CudaEventTimerScope
{
private:
    TorchFuncs* m_torchFuncs = nullptr;
    CudaEventTimer* m_timer = nullptr;
public:
    CudaEventTimerScope(TorchFuncs& torchFuncs, CudaEventTimer& timer);
    CudaEventTimerScope(const CudaEventTimerScope& other) = delete;
    CudaEventTimerScope(CudaEventTimerScope&& old);
    CudaEventTimerScope& operator=(CudaEventTimerScope&& old);
    virtual ~CudaEventTimerScope();
    inline operator bool() const { return m_timer != nullptr; }
};

}}


#endif // STYLE_TRANSFER_MORPH_IMPL_CUDA_EVENT_TIMER_HPP