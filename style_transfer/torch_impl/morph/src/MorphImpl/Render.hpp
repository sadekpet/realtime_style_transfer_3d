#ifndef STYLE_TRANSFER_MORPH_IMPL_RENDER_HPP
#define STYLE_TRANSFER_MORPH_IMPL_RENDER_HPP

#include <string>

#include <MorphImpl/Torch/TorchFuncs.hpp>

namespace Morph { namespace MorphImpl {

void TorchStyleTransfer(const char* model, TorchFuncs torchFuncs);

}}


#endif // STYLE_TRANSFER_MORPH_IMPL_RENDER_HPP