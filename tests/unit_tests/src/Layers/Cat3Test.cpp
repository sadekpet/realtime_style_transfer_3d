#include <gtest/gtest.h>
#include <NeuralNet/Layers/Cat3.hpp>
#include <GlobalState.hpp>
#include <TestingUtils.hpp>

using namespace Morph;

TEST(LayersCat3, basicData_i1_i1_i1) {
    style_transfer::Cat3 cat3;
    cat3.set_id1(0);
    cat3.set_id2(0);
    cat3.set_id3(0);
    
    Cat3Program cat3Program(TestsGlobalState::neuralNetResources(), cat3);

    array<float, 4> inputValues1 = {
        1, 2,
        3, 4
    };
    array<float, 4> inputValues2 = {
        5, 6,
        7, 8
    };
    array<float, 4> inputValues3 = {
         9, 10,
        11, 12
    };

    Texture3D inputData1 = TestsUtils::CreateTexture3D(uvec3(2,2,1), inputValues1.data());
    Texture3D inputData2 = TestsUtils::CreateTexture3D(uvec3(2,2,1), inputValues2.data());
    Texture3D inputData3 = TestsUtils::CreateTexture3D(uvec3(2,2,1), inputValues3.data());
    Texture3D outputData = TestsUtils::CreateTexture3D(uvec3(2,2,3));

    cat3Program.Evaluate(TestsGlobalState::context(), inputData1, inputData2, inputData3, outputData);

    const int outSize = 12;
    array<float, outSize> expectedOutputValues = {
        1, 2,
        3, 4,

        5, 6,
        7, 8,

         9, 10,
        11, 12
    };
    array<float, outSize> actualOutputValues;
    outputData.GetData(actualOutputValues.data());

    ASSERT_PRED3(TestsUtils::CompareFloatsN<outSize>, actualOutputValues, expectedOutputValues, TestsUtils::precision());
}

TEST(LayersCat3, basicData_i2_i1_i1) {
    style_transfer::Cat3 cat3;
    cat3.set_id1(0);
    cat3.set_id2(0);
    cat3.set_id3(0);
    
    Cat3Program cat3Program(TestsGlobalState::neuralNetResources(), cat3);

    array<float, 8> inputValues1 = {
        1, 2,
        3, 4,

        5, 6,
        7, 8
    };
    array<float, 4> inputValues2 = {
         9, 10,
        11, 12
    };
    array<float, 4> inputValues3 = {
        13, 14,
        15, 16
    };

    Texture3D inputData1 = TestsUtils::CreateTexture3D(uvec3(2,2,2), inputValues1.data());
    Texture3D inputData2 = TestsUtils::CreateTexture3D(uvec3(2,2,1), inputValues2.data());
    Texture3D inputData3 = TestsUtils::CreateTexture3D(uvec3(2,2,1), inputValues3.data());
    Texture3D outputData = TestsUtils::CreateTexture3D(uvec3(2,2,4));

    cat3Program.Evaluate(TestsGlobalState::context(), inputData1, inputData2, inputData3, outputData);

    const int outSize = 16;
    array<float, outSize> expectedOutputValues = {
        1, 2,
        3, 4,

        5, 6,
        7, 8,

         9, 10,
        11, 12,

        13, 14,
        15, 16
    };
    array<float, outSize> actualOutputValues;
    outputData.GetData(actualOutputValues.data());

    ASSERT_PRED3(TestsUtils::CompareFloatsN<outSize>, actualOutputValues, expectedOutputValues, TestsUtils::precision());
}

TEST(LayersCat3, basicData_i1_i2_i1) {
    style_transfer::Cat3 cat3;
    cat3.set_id1(0);
    cat3.set_id2(0);
    cat3.set_id3(0);
    
    Cat3Program cat3Program(TestsGlobalState::neuralNetResources(), cat3);

    array<float, 4> inputValues1 = {
        1, 2,
        3, 4
    };
    array<float, 8> inputValues2 = {
        5, 6,
        7, 8,

         9, 10,
        11, 12
    };
    array<float, 4> inputValues3 = {
        13, 14,
        15, 16
    };

    Texture3D inputData1 = TestsUtils::CreateTexture3D(uvec3(2,2,1), inputValues1.data());
    Texture3D inputData2 = TestsUtils::CreateTexture3D(uvec3(2,2,2), inputValues2.data());
    Texture3D inputData3 = TestsUtils::CreateTexture3D(uvec3(2,2,1), inputValues3.data());
    Texture3D outputData = TestsUtils::CreateTexture3D(uvec3(2,2,4));

    cat3Program.Evaluate(TestsGlobalState::context(), inputData1, inputData2, inputData3, outputData);

    const int outSize = 16;
    array<float, outSize> expectedOutputValues = {
        1, 2,
        3, 4,

        5, 6,
        7, 8,

         9, 10,
        11, 12,

        13, 14,
        15, 16
    };
    array<float, outSize> actualOutputValues;
    outputData.GetData(actualOutputValues.data());

    ASSERT_PRED3(TestsUtils::CompareFloatsN<outSize>, actualOutputValues, expectedOutputValues, TestsUtils::precision());
}

TEST(LayersCat3, basicData_i1_i1_i2) {
    style_transfer::Cat3 cat3;
    cat3.set_id1(0);
    cat3.set_id2(0);
    cat3.set_id3(0);
    
    Cat3Program cat3Program(TestsGlobalState::neuralNetResources(), cat3);

    array<float, 4> inputValues1 = {
        1, 2,
        3, 4,
    };
    array<float, 4> inputValues2 = {
        5, 6,
        7, 8
    };
    array<float, 8> inputValues3 = {
         9, 10,
        11, 12,

        13, 14,
        15, 16
    };

    Texture3D inputData1 = TestsUtils::CreateTexture3D(uvec3(2,2,1), inputValues1.data());
    Texture3D inputData2 = TestsUtils::CreateTexture3D(uvec3(2,2,1), inputValues2.data());
    Texture3D inputData3 = TestsUtils::CreateTexture3D(uvec3(2,2,2), inputValues3.data());
    Texture3D outputData = TestsUtils::CreateTexture3D(uvec3(2,2,4));

    cat3Program.Evaluate(TestsGlobalState::context(), inputData1, inputData2, inputData3, outputData);

    const int outSize = 16;
    array<float, outSize> expectedOutputValues = {
        1, 2,
        3, 4,

        5, 6,
        7, 8,

         9, 10,
        11, 12,

        13, 14,
        15, 16
    };
    array<float, outSize> actualOutputValues;
    outputData.GetData(actualOutputValues.data());

    ASSERT_PRED3(TestsUtils::CompareFloatsN<outSize>, actualOutputValues, expectedOutputValues, TestsUtils::precision());
}

TEST(LayersCat3, basicData_i1_i2_i3) {
    style_transfer::Cat3 cat3;
    cat3.set_id1(0);
    cat3.set_id2(0);
    cat3.set_id3(0);
    
    Cat3Program cat3Program(TestsGlobalState::neuralNetResources(), cat3);

    array<float, 4> inputValues1 = {
        1, 2,
        3, 4
    };
    array<float, 8> inputValues2 = {
        5, 6,
        7, 8,

         9, 10,
        11, 12
    };
    array<float, 12> inputValues3 = {
        13, 14,
        15, 16,

        17, 18,
        19, 20,

        21, 22,
        23, 24
    };

    Texture3D inputData1 = TestsUtils::CreateTexture3D(uvec3(2,2,1), inputValues1.data());
    Texture3D inputData2 = TestsUtils::CreateTexture3D(uvec3(2,2,2), inputValues2.data());
    Texture3D inputData3 = TestsUtils::CreateTexture3D(uvec3(2,2,3), inputValues3.data());
    Texture3D outputData = TestsUtils::CreateTexture3D(uvec3(2,2,6));

    cat3Program.Evaluate(TestsGlobalState::context(), inputData1, inputData2, inputData3, outputData);

    const int outSize = 24;
    array<float, outSize> expectedOutputValues = {
        1, 2,
        3, 4,

        5, 6,
        7, 8,

         9, 10,
        11, 12,

        13, 14,
        15, 16,

        17, 18,
        19, 20,

        21, 22,
        23, 24
    };
    array<float, outSize> actualOutputValues;
    outputData.GetData(actualOutputValues.data());

    ASSERT_PRED3(TestsUtils::CompareFloatsN<outSize>, actualOutputValues, expectedOutputValues, TestsUtils::precision());
}