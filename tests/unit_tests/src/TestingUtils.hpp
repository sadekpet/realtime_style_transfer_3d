#ifndef STYLE_TRANSFER_UNIT_TESTS_UTILS_HPP
#define STYLE_TRANSFER_UNIT_TESTS_UTILS_HPP

#include "GlobalState.hpp"
#include <NeuralNet/nndata.pb.h>

#include <cstdlib>

class TestsUtils
{
public:

    inline static float precision() { return 1e-04f; }

    //static Morph::Texture3D CreateTexture(Morph::uvec3 dim, Morph::TextureSizedFormat format);
    template<size_t N>
    inline static bool CompareFloatsN(Morph::array<float, N> actual, Morph::array<float, N> expected, float eps) 
    {
        bool res = true;
        for(size_t i = 0; i < N; ++i) {
            res = res && abs(actual[i] - expected[i]) < eps;
        }
        return res;
    }

    inline static bool CompareFloats(Morph::vector<float> actual, Morph::vector<float> expected, float eps) 
    {
        if(actual.size() != expected.size()) {
            return false;
        }
        bool res = true;
        for(size_t i = 0; i < expected.size(); ++i) {
            res = res && abs(actual[i] - expected[i]) < eps;
        }
        return res;
    }

    static Morph::Texture2D CreateTexture2D(Morph::uvec2 dim);
    static Morph::Texture2D CreateTexture2D(Morph::uvec2 dim, const void* data);

    static Morph::Texture3D CreateTexture3D(Morph::uvec3 dim);
    static Morph::Texture3D CreateTexture3D(Morph::uvec3 dim, const void* data);

    template<typename IteratorType>
    inline static void GenRandFloats(IteratorType first, IteratorType last, float min = -1, float max = 1) {
        std::transform(first, last, first, [&](float val) { return (max - min) * (static_cast<float>(std::rand()) / RAND_MAX) + min; });
    }

    inline static Morph::vector3d<float> GenRandVector3d(Morph::uvec3 dim, float min = -1, float max = 1) {
        Morph::vector3d<float> res(dim);
        GenRandFloats(res.begin(), res.end(), min, max);
        return res;
    }

    static style_transfer::Conv2d CretateConv2d(Morph::uint in_channels, Morph::uint out_channels, Morph::uvec2 kernel_size, Morph::vector<float> weights, Morph::vector<float> biases = Morph::vector<float>(), Morph::uvec2 stride = Morph::uvec2(1), bool use_bias = true);
    static style_transfer::Conv2d CretateUniformConv2d(Morph::uint in_channels, Morph::uint out_channels, Morph::uvec2 kernel_size, Morph::uvec2 stride = Morph::uvec2(1), bool use_bias = true, float weights = 1, float biases = 0);
    static style_transfer::Conv2d CretateRandomConv2d(Morph::uint in_channels, Morph::uint out_channels, Morph::uvec2 kernel_size, Morph::uvec2 stride = Morph::uvec2(1), bool use_bias = true);
};

#endif // STYLE_TRANSFER_UNIT_TESTS_UTILS_HPP