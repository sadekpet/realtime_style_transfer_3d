#include <iostream>
#include <math.h>

#include <MorphImpl/Tests/TorchTest.hpp>
#include <TorchAPI.hpp>

int main(void)
{
  using namespace Morph::TorchImpl;
  
  Morph::MorphImpl::TorchTest(Morph::GetTorchAPI());
  
  return 0;
} 
 
